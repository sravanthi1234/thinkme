import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MakeupPage } from './makeup';
import { CalendarModule } from "ion2-calendar";

@NgModule({
  declarations: [
    MakeupPage,
  ],
  imports: [
    IonicPageModule.forChild(MakeupPage),CalendarModule,
  ],
})
export class MakeupPageModule {}
