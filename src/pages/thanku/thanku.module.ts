import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ThankuPage } from './thanku';

@NgModule({
  declarations: [
    ThankuPage,
  ],
  imports: [
    IonicPageModule.forChild(ThankuPage),
  ],
})
export class ThankuPageModule {}
