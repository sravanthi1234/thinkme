import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MehandiDetailsPage } from './mehandi-details';

@NgModule({
  declarations: [
    MehandiDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(MehandiDetailsPage),
  ],
})
export class MehandiDetailsPageModule {}
