import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CourierDetailsInfo} from '../../Model/CourierDetailsInfo';
import { CourierDataProvider } from '../../providers/courier-data/courier-data';
import 'rxjs/Rx';
/**
 * Generated class for the MybookingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'mybookings',
  templateUrl: 'mybookings.html',
})
export class MybookingsPage {
  courierDetailsInfo:CourierDetailsInfo[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public _courierDataProvider:CourierDataProvider) {
  }
  ngOnInit() {
    this._courierDataProvider.getMyHistry()
    .subscribe(courierDetails => this.courierDetailsInfo = courierDetails,
    error => { console.error(error) });
    
  }

  ionViewDidLoad() {


  }

}
