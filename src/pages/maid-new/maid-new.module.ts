import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaidNewPage } from './maid-new';

@NgModule({
  declarations: [
    MaidNewPage,
  ],
  imports: [
    IonicPageModule.forChild(MaidNewPage),
  ],
})
export class MaidNewPageModule {}
