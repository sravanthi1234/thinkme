import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BaratBandsPage } from './barat-bands';
import { CalendarModule } from "ion2-calendar";


@NgModule({
  declarations: [
    BaratBandsPage,
  ],
  imports: [
    IonicPageModule.forChild(BaratBandsPage),CalendarModule,
  ],
})
export class BaratBandsPageModule {}
