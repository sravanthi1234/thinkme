import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular'
import { FoodTotalPage } from '../food-total/food-total';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the VegPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-veg',
  templateUrl: 'veg.html',
})
export class VegPage {
  wasteChecked: any = [];
  all:any;
   allservices= [
    [{label:'Dal', imgUrl:'assets/serviceslogo/ss.jpg',cost:'Rs.290'}],
    [{label:'Tomato Curry', imgUrl:'assets/serviceslogo/ss.jpg',cost:'Rs.399'}],
    [{label:'Brinjaal Curry', imgUrl:'assets/serviceslogo/ss.jpg',cost:'Rs.199'}],
  [{label:'Potato Fry', imgUrl:'assets/serviceslogo/ss.jpg',cost:'Rs.290'}],
   ];
  constructor(public navCtrl: NavController,private storage: Storage,public actionSheetCtrl: ActionSheetController, public navParams: NavParams) {
  }

  
  ionViewDidLoad() {
    console.log('ionViewDidLoad VegPage');
   
  }
  foodTotal(){
    console.log(this.wasteChecked);
    this.presentActionSheet();
   

  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Price',
      buttons: [
        {
          text: 'Your  Total Cost is :-' + " Rs.1500"
        },
        
      
      
        
        {
          text: 'Confirm',
          role: 'conform',cssClass:'btnConfirmClass',
          handler: () => {
            this.gotototal();
          }
        }
      ]
    });
    actionSheet.present();
  }
 
  datachanged(e:any,data){
    //console.log(e);
    console.log(data);
    if(e.checked==true) {
      console.log("dd")
      this.wasteChecked.push(data);
      console.log(this.wasteChecked);
  }else if(e.checked==false){
    let index = this.wasteChecked.indexOf(data);
      console.log(index + "this is index"+data);
      // if (index == -1) {
        this.wasteChecked.splice(index, 1);
        console.log(this.wasteChecked);
      // }

  }
  this.storage.set('vegNew', this.wasteChecked);
}
gotototal(){
  this.navCtrl.push(FoodTotalPage,{categoryveg:this.wasteChecked});
  
}
}
