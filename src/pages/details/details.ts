import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {
parameter1:any;
  parameter2: any;
  parameter3:any;
  parameter4:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  
    this.parameter1 = navParams.get('param1'); 
  
    console.log(this.parameter1)
    this.parameter2 = navParams.get('param2'); 
    this.parameter3 = navParams.get('param3'); 
    this.parameter4 = navParams.get('courierInfo'); 
    console.log("dd");
    console.log(this.parameter4);
    console.log(this.parameter4);
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsPage');
  }

}
