import { SpaDetailsPage } from './../spa-details/spa-details';

import { PlannersPage } from './../planners/planners';
import { DjPage } from './../dj/dj';
import { BaratBandsPage } from './../barat-bands/barat-bands';
import { VenueBookingPage } from './../venue-booking/venue-booking';
import { MehandiPage } from './../mehandi/mehandi';
import { WeddingCarsPage } from './../wedding-cars/wedding-cars';
import { MakeupPage } from './../makeup/makeup';
import { CookingPage } from './../cooking/cooking';
import { BabyCarePage } from './../baby-care/baby-care';
import { MaidNewPage } from './../maid-new/maid-new';
import { MaidPage } from './../maid/maid';
import { SportsPage } from './../sports/sports';
import { AddressPage } from './../address/address';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Item } from 'ionic-angular';
import { LocationComponent } from '../../components/location/location';
import { EducationPage } from '../education/education';
import { ElderCarePage } from '../elder-care/elder-care';
import { DecorationPage } from '../decoration/decoration';
import { PhotographyPage } from '../photography/photography';
import { NursePage } from '../nurse/nurse';

/**
 * Generated class for the SubCategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sub-category',
  templateUrl: 'sub-category.html',
})
export class SubCategoryPage {
categoryName:any;
allservices=[];
 subheading=[];
 banner=[];
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    

  }
  booking(name) {
    
    if(this.categoryName=='appliances'){
      this.navCtrl.push(LocationComponent,{category:name});
      console.log(name);
    }
    else if(this.categoryName=='tutors'){
      if(name == "Sports"){
        console.log(name);
        this.navCtrl.push('SportsPage');
        console.log(name)
      }else if(name == "Education"){
        this.navCtrl.push('EducationPage');
        console.log(name)
      }
     
     
    }
    else if(this.categoryName=='maid'){
      if(name == "Maid"){
        console.log(name);
        this.navCtrl.push('MaidNewPage');
        console.log(name)
      }else if(name == "Baby Sitting"){
        console.log(name);
        this.navCtrl.push('BabyCarePage');
        console.log(name)
      }
      else if(name == "Cooking"){
        console.log(name);
        this.navCtrl.push('CookingPage');
        console.log(name)
      }
      else if(name == "Elder Care"){
        console.log(name);
        this.navCtrl.push('ElderCarePage');
        console.log(name)
      }
     
     
    }
    else if(this.categoryName=='events'){
      if(name == "Decorations"){
        console.log(name);
        this.navCtrl.push('DecorationPage');
        console.log(name)
      }else if(name == "Make-Up"){
        console.log(name);
        this.navCtrl.push('MakeupPage');
        console.log(name)
      }
      else if(name == "Wedding Cars"){
        console.log(name);
        this.navCtrl.push('WeddingCarsPage');
        console.log(name)
      }
      else if(name == "Photography"){
        console.log(name);
        this.navCtrl.push('PhotographyPage');
        console.log(name)
      }
      else if(name == "Mehandi"){
        console.log(name);
        this.navCtrl.push('MehandiPage');
        console.log(name)
      }
      else if(name == "Venue Booking"){
        console.log(name);
        this.navCtrl.push('VenueBookingPage');
        console.log(name)
      }
      else if(name == "Baaraat Bands"){
        console.log(name);
        this.navCtrl.push('BaratBandsPage');
        console.log(name)
      }
      else if(name == "DJ & Lighting"){
        console.log(name);
        this.navCtrl.push('DjPage');
        console.log(name)
      }
      else if(name == "Wedding Planners"){
        console.log(name);
        this.navCtrl.push('PlannersPage');
        console.log(name)
      }
     
     
     
    }
    else if(this.categoryName=='elder'){
      if(name == "Maid Services"){
        console.log(name);
        this.navCtrl.push('ElderCarePage');
        console.log(name);
      }else if(name == "Nurse Services"){
        this.navCtrl.push('NursePage');
        console.log(name);
      }
     
     
    }
    else if(this.categoryName=='beauty'){
      this.navCtrl.push('SpaDetailsPage',{category:name});
      console.log(name);
     
     
    }
    
  }
  ionViewDidLoad() {
    
    this.categoryName=this.navParams.get("category");
    console.log(this.categoryName)
    if(this.categoryName=='home'){
     this.subheading= [
        {label:'Appliance Services', imgUrl:'assets/serviceslogo/title.png'} ,
      
      ];
      this.banner= [
        { imgUrl:'assets/serviceslogo/slide5.png'} ,
      
      ];


     this. allservices= [
        [{label:'Washing Machine', imgUrl:'assets/serviceslogo/22.png'}, {label:'Air Conditioner', imgUrl:'assets/serviceslogo/23.png'}],
        [{label:'Gas Stove', imgUrl:'assets/serviceslogo/24.png'}, {label:'Refrigerator', imgUrl:'assets/serviceslogo/25.png'}],
        [{label:'Geyser', imgUrl:'assets/serviceslogo/19.png'}, {label:'Cooler', imgUrl:'assets/serviceslogo/20.png'}],
      [{label:'Water Purifier', imgUrl:'assets/serviceslogo/21.png'}, {label:'Refrigerator', imgUrl:'assets/serviceslogo/21.png'}],
       ];

    }
    else if(this.categoryName=='appliances'){
      this.subheading= [
        {label:'Appliance Services', imgUrl:'assets/serviceslogo/title.png'} ,
      
      ];
      this.banner= [
        { imgUrl:'assets/serviceslogo/slide5.png'} ,
      
      ];
      this. allservices= [
        [{label:'Washing Machine', imgUrl:'assets/serviceslogo/22.png'}, {label:'Air Conditioner', imgUrl:'assets/serviceslogo/23.png'}],
        [{label:'Gas Stove', imgUrl:'assets/serviceslogo/24.png'}, {label:'Refrigerator', imgUrl:'assets/serviceslogo/25.png'}],
        [{label:'Geyser', imgUrl:'assets/serviceslogo/19.png'}, {label:'Cooler', imgUrl:'assets/serviceslogo/20.png'}],
      [{label:'Water Purifier', imgUrl:'assets/serviceslogo/21.png'}, {label:'Refrigerator', imgUrl:'assets/serviceslogo/21.png'}],
 
      ];
 
     }
     else if(this.categoryName=='tutors'){
      this.subheading= [
        {label:'Tutors', imgUrl:'assets/serviceslogo/tutor.png'} ,
      
      ];
      this.banner= [
        { imgUrl:'assets/serviceslogo/banner6.jpg'} ,
      
      ];
      this. allservices= [
        [{label:'Education', imgUrl:'assets/events/education.png'}, {label:'Sports', imgUrl:'assets/events/sports.png'}]
       
      ];
 
     }
     else if(this.categoryName=='elder'){
      this.subheading= [
        {label:'Tutors', imgUrl:'assets/serviceslogo/title.png'} ,
      
      ];
      this.banner= [
        { imgUrl:'assets/serviceslogo/bg1.jpg'} ,
      
      ];
      this. allservices= [
        [{label:'Maid Services', imgUrl:'assets/serviceslogo/22.png'}, {label:'Nurse Services', imgUrl:'assets/serviceslogo/23.png'}],
       
      
      ];
 
     }
     else if(this.categoryName=='maid'){
      this.subheading= [
        {label:'Maid Services', imgUrl:'assets/serviceslogo/title.png'} ,
      
      ];
      this.banner= [
        { imgUrl:'assets/serviceslogo/background.jpeg'} ,
      
      ];
      this. allservices= [
        [{label:'Maid', imgUrl:'assets/serviceslogo/22.png'}, {label:'Baby Sitting', imgUrl:'assets/serviceslogo/23.png'}],
        [{label:'Cooking', imgUrl:'assets/serviceslogo/22.png'}, {label:'Elder Care', imgUrl:'assets/serviceslogo/23.png'}],
      
      ];
 
     }
     else if(this.categoryName=='events'){
      this.subheading= [
        {label:'Events', imgUrl:'assets/serviceslogo/events.png'} ,
      
      ];
      this.banner= [
        { imgUrl:'assets/serviceslogo/banner7.jpg'} ,
      
      ];
      this. allservices= [
        [{label:'Decorations', imgUrl:'assets/events/decorations.png'}, {label:'Make-Up', imgUrl:'assets/events/makeup.png'}],
        [ {label:'Wedding Cars', imgUrl:'assets/events/wedding car.png'},{label:'Photography', imgUrl:'assets/events/photography.png'}],
        [{label:'Mehandi', imgUrl:'assets/events/mehandi.png'}, {label:'Venue Booking', imgUrl:'assets/events/venu booking.png'}],
       
        [{label:'Baaraat Bands', imgUrl:'assets/events/bands.png'},{label:'Wedding Planners', imgUrl:'assets/events/wedding planners.png'}],
        [{label:'DJ & Lighting', imgUrl:'assets/events/dj lighting.png'}]
     
      ];
 
     }
     else if(this.categoryName=='health'){
      this.subheading= [
        {label:'Health & Fitness', imgUrl:'assets/serviceslogo/title.png'} ,
      
      ];
      this.banner= [
        { imgUrl:'assets/serviceslogo/intro.jpg'} ,
      
      ];
      this. allservices= [
        [{label:'Fitness Trainer', imgUrl:'assets/serviceslogo/22.png'}, {label:'Dietician', imgUrl:'assets/serviceslogo/23.png'}],
        [{label:'Yoga Trainer', imgUrl:'assets/serviceslogo/22.png'}, {label:'physiotherapy', imgUrl:'assets/serviceslogo/23.png'}],
       
      
      ];
 
     }
     else if(this.categoryName=='beauty'){
      this.subheading= [
        {label:'Beauty & Spa', imgUrl:'assets/serviceslogo/title.png'} ,
      
      ];
      this.banner= [
        { imgUrl:'assets/serviceslogo/ss1.jpg'} ,
      
      ];
      this.allservices= [
    
        [{label:'Rica Waxing', imgUrl:'assets/serviceslogo/rica waxing.png'},{label:'Regular Waxing', imgUrl:'assets/serviceslogo/regular waxing.png'}],
    
        [ {label:'Facial', imgUrl:'assets/serviceslogo/facial.png'},{label:'Pedicure', imgUrl:'assets/serviceslogo/pedicure.png'}],
        [{label:'Manicure', imgUrl:'assets/serviceslogo/manicure.png'}, {label:'Hair', imgUrl:'assets/serviceslogo/hair.png'}],
      [{label:'Threading', imgUrl:'assets/serviceslogo/threading.png'},{label:'Bleach', imgUrl:'assets/serviceslogo/bleach detan.png'}],
      [{label:'Detan', imgUrl:'assets/serviceslogo/bleach detan.png'}],
       ];
 
     }
     
 
    console.log('ionViewDidLoad SubCategoryPage');
  }

}
