import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoodTotalPage } from './food-total';

@NgModule({
  declarations: [
    FoodTotalPage,
  ],
  imports: [
    IonicPageModule.forChild(FoodTotalPage),
  ],
})
export class FoodTotalPageModule {}
