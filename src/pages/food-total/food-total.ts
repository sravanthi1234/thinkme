import { HomePage } from './../home/home';
import { ThinkmeProvider } from './../../providers/thinkme/thinkme';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { constructDependencies } from '@angular/core/src/di/reflective_provider';

import { ToastController } from 'ionic-angular';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the FoodTotalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-food-total',
  templateUrl: 'food-total.html',
})
export class FoodTotalPage {
people:any;
date:any;
time:any;
veg:any =[];
nonVeg:any =[];
 vegNew:any=[];
nvnew:any=[];
veggie:any=[];
nonveggie:any=[];
vegFinal:any;
nonFinal:any;
plates:number;
location:any;
username:any;
useremail:any;
phoneNumber:any;
  constructor(public navCtrl: NavController, private alertCtrl: AlertController,public loadingCtrl: LoadingController,public usersService: ThinkmeProvider, public navParams: NavParams,public http: HttpClient,public toastCtrl: ToastController,private storage: Storage) {
   this.plates= this.navParams.get("category");
   this.time= this.navParams.get("category1");

   this.date= this.navParams.get("category2");
   console.log(this.plates);
   this.location= this.navParams.get("category3");
   this.storage.get('username').then((val) => {
    console.log('Your name is', val);
    this.username=val;
  });
  this.storage.get('useremail').then((val) => {
    console.log('Your email is', val);
    this.useremail=val;
  });
  this.storage.get('phone').then((val) => {
    console.log('Your phone Number is', val);
    this.phoneNumber=val;
  });
  //  this.nonVeg= this.navParams.get("categorynon");
  
  // console.log(this.nonVeg);


  
   
 
 
//     this.storage.get('vegNew').then((val) => {
//       console.log('Your final veg is', val);
//       this.vegNew=val;
//     console.log(this.vegNew);
//     for(let i = 0; i < this.vegNew.length; i++){
//       this.veggie  = this.vegNew;
//       console.log("new")
//      console.log(this.veggie[i]);
//      console.log("stop")
// } 
   

//     });
//     this.storage.get('nonveg').then((val) => {
//       console.log('Your final nonveg is', val);
//       this.nvnew=val;
//     console.log(this.nvnew);
//     for(let i = 0; i < this.nvnew.length; i++){
//       this.nonveggie  = this.nvnew;
//       console.log("new")
//      console.log(this.nonveggie[i]);
//      console.log("stop")
// } 
   
//     });
   
  }

  ionViewDidLoad() {
  
    console.log('ionViewDidLoad FoodTotalPage');
  }
  logForm() {
      
    //console.log(this.todo.value)
    let body ={
      "userid":this.username,
"eventCategory": "Catering",
 "mainCategory": "Catering",
  "subCategory":"Catering",
  "Date": this.date,
  "Time":this.time,
 "bookingData":
      {
        "userid":this.username,
        "mainCategory": "Catering",
        "subCategory":"Catering",
        "userEmail": this.useremail,
        "phoneNumber":  this.phoneNumber,
        "Date": this.date,
        "Time":this.time,
        "No_Plates":this.plates,
        "Location":this.location
        
      }
      }
      
  console.log(body);


this.usersService.bookingUser(body).subscribe(res => {
console.log(JSON.stringify(res["Message"]));
console.log(res);
let responseMessage = res["Message"];
if(responseMessage){
  // let toast = this.toastCtrl.create({
  //   message: responseMessage,
  //   duration: 3000,
  //   cssClass: "yourCssClassName",
  //   position: 'bottom'
  // });
  let alert = this.alertCtrl.create({
    title: 'Thank you.!!',
    subTitle: responseMessage,
    buttons: ['ok']
  });
  alert.onDidDismiss(() => {
    // this.loading.dismiss();
    this.navCtrl.setRoot(HomePage);
   });
//this.loading.present();
alert.present();
}else{

}
})



  
  }
}
