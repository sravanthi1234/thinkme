import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DjDetailsPage } from './dj-details';

@NgModule({
  declarations: [
    DjDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(DjDetailsPage),
  ],
})
export class DjDetailsPageModule {}
