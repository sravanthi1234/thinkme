import { LoginPage } from './../login/login';
import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Nav, Platform,Events } from 'ionic-angular';
/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  username:any;
  useremail:any;
  phoneNumber:any;
  rootPage:any;
  constructor(public navCtrl: NavController,public events:Events,private storage: Storage, public menuCtrl: MenuController,public navParams: NavParams) {
    this.storage.get('username').then((val) => {
      console.log('Your name is', val);
      this.username=val;
    });
    console.log(this.username);
    this.storage.get('useremail').then((val) => {
      console.log('Your email is', val);
      this.useremail=val;
    });
    this.storage.get('phone').then((val) => {
      console.log('Your phone Number is', val);
      this.phoneNumber=val;
    });
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }
  home(){
    this.navCtrl.setRoot(HomePage);
   }
    openPage(page) {
      this.navCtrl.setRoot(page.component);
    }
    logout(){
      this.navCtrl.setRoot(LoginPage);
      this.menuCtrl.close();
    }
    menuClosed() {
      this.events.publish('menu:closed', '');
    }
    menuOpened() {
      this.events.publish('menu:opened', '');
    }
}
