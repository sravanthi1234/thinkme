import { SpaNewPage } from './../spa-new/spa-new';
import { SpaDetailsPage } from './../spa-details/spa-details';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { DepFlags } from '@angular/compiler/src/core';
/**
 * Generated class for the SpaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-spa',
  templateUrl: 'spa.html',
})
export class SpaPage {
  option:any;
  cost:any;
  lengthnew:any;
  selectedSpa:any;
  regularWax:any=[];
  wasteChecked:any=[];
  length:any;
  finallArry:any=[];
 allservices:any=[];
 Categories:any=[{name:'Pedicure',status:false},
 {name:'Manicure',status:false},
 {name:'Hair',status:false},
 {name:'Threading',status:false},
 {name:'Bleach',status:false},
 {name:'Detan',status:false}]
   recawax= [
    {label:'Half arms waxing Rs.230'},
    {label:'Half legs waxing  Rs.300'},
    {label:'Stomach waxing Rs. 350'},
    {label:'Back waxing Rs. 400'}  
  
   
  ];
  
  buttonColor: string = '#fff'; //Default Color
  buttonColor1: string = '#fff'; //Default Color
  buttonColor2: string = '#fff'; //Default Color
  buttonColor3: string = '#fff'; //Default Color
  buttonColor4: string = '#fff'; //Default Color
  buttonColor5: string = '#fff'; //Default Color
  buttonColor6: string = '#fff'; //Default Color
  buttonColor7: string = '#fff'; //Default Color
  buttonColor8: string = '#fff'; //Default Color
  fontColor:string='#0c4393';
  fontColor1: string = '#0c4393'; //Default Color
  fontColor2: string = '#0c4393'; //Default Color
  fontColor3: string = '#0c4393'; //Default Color
  fontColor4: string = '#0c4393'; //Default Color
  fontColor5: string = '#0c4393'; //Default Color
  fontColor6: string = '#0c4393'; //Default Color
  fontColor7: string = '#0c4393'; //Default Color
  fontColor8: string = '#0c4393'; //Default Color
  constructor(public navCtrl: NavController, public navParams: NavParams,private storage: Storage) {
  //   this.ricaWax=[];
  // this.finallArry=[];
 
  //   // this.lengthnew=this.selectedSpa.length;
  //   // console.log(this.selectedSpa.length)
  //  storage.get('regular').then((val) => {
  //     // console.log('rica wax is', val);
  //     this.ricaWax=val;
  //     console.log(this.ricaWax)
    
  //   });
    
  // storage.get('rica').then((val) => {
  //   this.regualrWax=val;
  //   console.log(this.regualrWax);

  //   });
 
   
  }
  getcategory(item){
console.log(item);
item.status=true;
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SpaPage');
  }
  spa(label){
   
    console.log(label);
    this.option=label;
    if(this.option=='Rica Waxing'){
     
      console.log("ss")
    }
   
    // this.navCtrl.push(SpaDetailsPage,{category:this.option})

  }
  rica(){
    this.buttonColor = '#0c4393'; 
    this.buttonColor1 = '#fff'; 
    this.buttonColor2 = '#fff'; 
    this.buttonColor3 = '#fff'; 
    this.buttonColor4 = '#fff'; 
    this.buttonColor5 = '#fff'; 
    this.buttonColor6 = '#fff'; 
    this.buttonColor7 = '#fff'; 
    this.buttonColor8 = '#fff'; 
    this.fontColor='#fff';
    this.fontColor1 = '#0c4393'; 
    this.fontColor2 = '#0c4393'; 
    this.fontColor3 = '#0c4393'; 
    this.fontColor4 = '#0c4393'; 
    this.fontColor5 = '#0c4393'; 
    this.fontColor6 = '#0c4393'; 
    this.fontColor7 = '#0c4393'; 
    this.fontColor8 = '#0c4393'; 
    this.allservices= [
      {label:'Half arms waxing Rs.230'},
      {label:'Half legs waxing  Rs.300'},
      {label:'Stomach waxing Rs. 350'},
      {label:'Back waxing Rs. 400'}  
    
     
    ];
  }
  regular(){
    this.buttonColor = '#fff';
    this.buttonColor1 = '#0c4393'; 
    this.buttonColor2 = '#fff'; 
    this.buttonColor3 = '#fff';
    this.buttonColor4 = '#fff'; 
    this.buttonColor5 = '#fff'; 
    this.buttonColor6 = '#fff'; 
    this.buttonColor7 = '#fff'; 
    this.buttonColor8 = '#fff'; 
    this.fontColor='#0c4393';
    this.fontColor1 = '#fff'; 
    this.fontColor2 = '#0c4393'; 
    this.fontColor3 = '#0c4393'; 
    this.fontColor4 = '#0c4393'; 
    this.fontColor5 = '#0c4393'; 
    this.fontColor6 = '#0c4393'; 
    this.fontColor7 = '#0c4393'; 
    this.fontColor8 = '#0c4393'; 
    this.allservices= [
      {label:'Full arms (rica )+underarms (peal off)waxing  Rs.500'},
      {label:'Full legs (No B-Line and No Brazilian) Rs.500'},
      {label:'Full body wax (No B -wax) Rs.1100'},
      {label:'Underarms peal off wax Rs.150'},
      {label:'B-Line wax Rs.150'},
      {label:'B-wax Rs.699'}
    
     
    ];

  }
  facial(){
    this.buttonColor = '#fff';
    this.buttonColor1 = '#fff'; 
    this.buttonColor2 = '#0c4393'; 
    this.buttonColor3 = '#fff';
    this.buttonColor4 = '#fff'; 
    this.buttonColor5 = '#fff'; 
    this.buttonColor6 = '#fff'; 
    this.buttonColor7 = '#fff'; 
    this.buttonColor8 = '#fff'; 
    this.fontColor='#0c4393';
    this.fontColor1 = '#0c4393'; 
    this.fontColor2 = '#fff'; 
    this.fontColor3 = '#0c4393'; 
    this.fontColor4 = '#0c4393'; 
    this.fontColor5 = '#0c4393'; 
    this.fontColor6 = '#0c4393'; 
    this.fontColor7 = '#0c4393'; 
    this.fontColor8 = '#0c4393'; 
    this.allservices= [
      {label:'Sara. Fruit clean up Rs 500'},
      {label:'Oil control treatment Rs.900'},
      {label:'Glovite facial Rs.950'},
      {label:'Tan clear  facial Rs.1200'},
      {label:'Feel youthful facial Rs.1200'},
      {label:'Shone &glow facial  Rs.1400'},
      {label:'Power brightening facial Rs.1500'}
    ];
  
 }
 pedicure(){
  this.buttonColor = '#fff';
  this.buttonColor1 = '#fff'; 
  this.buttonColor2 = '#fff'; 
  this.buttonColor3 = '#0c4393';
  this.buttonColor4 = '#fff'; 
    this.buttonColor5 = '#fff'; 
    this.buttonColor6 = '#fff'; 
    this.buttonColor7 = '#fff'; 
    this.buttonColor8 = '#fff'; 
    this.fontColor='#0c4393';
    this.fontColor1 = '#0c4393'; 
    this.fontColor2 = '#0c4393'; 
    this.fontColor3 = '#fff'; 
    this.fontColor4 = '#0c4393'; 
    this.fontColor5 = '#0c4393'; 
    this.fontColor6 = '#0c4393'; 
    this.fontColor7 = '#0c4393'; 
    this.fontColor8 = '#0c4393'; 
  this.allservices= [
    {label:'Cut,file&polish -Feet. Rs.150'},
    {label:'Classic pedicure Rs.530'},
    {label:'Glovite facial Rs.950'},
    {label:'Spa pedicure Rs.699'},
    {label:'Detan pedicure Rs.750'},
  ];
 }
 manicure(){
  this.buttonColor = '#fff';
  this.buttonColor1 = '#fff'; 
  this.buttonColor2 = '#fff'; 
  this.buttonColor3 = '#fff';
  this.buttonColor4 = '#0c4393'; 
    this.buttonColor5 = '#fff'; 
    this.buttonColor6 = '#fff'; 
    this.buttonColor7 = '#fff'; 
    this.buttonColor8 = '#fff'; 
    this.fontColor='#0c4393';
    this.fontColor1 = '#0c4393'; 
    this.fontColor2 = '#0c4393'; 
    this.fontColor3 = '#0c4393'; 
    this.fontColor4 = '#fff'; 
    this.fontColor5 = '#0c4393'; 
    this.fontColor6 = '#0c4393'; 
    this.fontColor7 = '#0c4393'; 
    this.fontColor8 = '#0c4393'; 
  this.allservices= [
    {label:'Cut,file&polish -Hand Rs150'},
    {label:'Classic manicure Rs.400'},
    {label:'Spa manicure Rs.500'},
    {label:'Spa pedicure Rs.699'},
    {label:'Detan manicure Rs.550'},
  ];
 }
 hair(){
  this.buttonColor = '#fff';
  this.buttonColor1 = '#fff'; 
  this.buttonColor2 = '#fff'; 
  this.buttonColor3 = '#fff';
  this.buttonColor4 = '#fff'; 
    this.buttonColor5 = '#0c4393'; 
    this.buttonColor6 = '#fff'; 
    this.buttonColor7 = '#fff'; 
    this.buttonColor8 = '#fff'; 
    this.fontColor='#0c4393';
    this.fontColor1 = '#0c4393'; 
    this.fontColor2 = '#0c4393'; 
    this.fontColor3 = '#0c4393'; 
    this.fontColor4 = '#0c4393'; 
    this.fontColor5 = '#fff'; 
    this.fontColor6 = '#0c4393'; 
    this.fontColor7 = '#0c4393'; 
    this.fontColor8 = '#0c4393'; 
  this.allservices= [
    {label:'Henna application Rs.400'},
    {label:'Hair colour application Rs.500'},
    {label:'Root touch up-upto 2 inches-Rs.699'},
    {label:'Hair spa up to waist length Rs.900'},
    {label:'Head massage Rs.250'},
    {label:'Global hair colour upto waist length Rs.2699'},
    {label:'Global hair colour below waist length Rs.3299'},
  ];
 }
 threading(){
  this.buttonColor = '#fff';
  this.buttonColor1 = '#fff'; 
  this.buttonColor2 = '#fff'; 
  this.buttonColor3 = '#fff';
  this.buttonColor4 = '#fff'; 
    this.buttonColor5 = '#fff'; 
    this.buttonColor6 = '#0c4393'; 
    this.buttonColor7 = '#fff'; 
    this.buttonColor8 = '#fff';  
    this.fontColor='#0c4393';
    this.fontColor1 = '#0c4393'; 
    this.fontColor2 = '#0c4393'; 
    this.fontColor3 = '#0c4393'; 
    this.fontColor4 = '#0c4393'; 
    this.fontColor5 = '#0c4393'; 
    this.fontColor6 = '#fff'; 
    this.fontColor7 = '#0c4393'; 
    this.fontColor8 = '#0c4393'; 
  this.allservices= [
    {label:'Eyebrow Rs.25'},
    {label:'Upper lip Rs.15'},
    {label:'Chin rs.25'},
    {label:'Forehead Rs.35'},
    {label:'Side locks Rs.40'},
    {label:'Full face Rs.130'},
   
  ];
 }
 bleach(){
  this.buttonColor = '#fff';
  this.buttonColor1 = '#fff'; 
  this.buttonColor2 = '#fff'; 
  this.buttonColor3 = '#fff';
  this.buttonColor4 = '#fff'; 
    this.buttonColor5 = '#fff'; 
    this.buttonColor6 = '#fff'; 
    this.buttonColor7 = '#0c4393'; 
    this.buttonColor8 = '#fff';  
    this.fontColor='#0c4393';
    this.fontColor1 = '#0c4393'; 
    this.fontColor2 = '#0c4393'; 
    this.fontColor3 = '#0c4393'; 
    this.fontColor4 = '#0c4393'; 
    this.fontColor5 = '#0c4393'; 
    this.fontColor6 = '#0c4393'; 
    this.fontColor7 = '#fff'; 
    this.fontColor8 = '#0c4393'; 
  this.allservices= [
    {label:'Face &Neck bleach Rs.350'},
    {label:'Full arms bleach Rs.300'},
    {label:'Full legs bleach Ra.400'},
    {label:'Front bleach Rs.350'},
    {label:'Back bleach Rs.350'},
    {label:'Full body bleach Rs.1399'},
  ];
 }
 detan(){
  this.buttonColor = '#fff';
  this.buttonColor1 = '#fff'; 
  this.buttonColor2 = '#fff'; 
  this.buttonColor3 = '#fff';
  this.buttonColor4 = '#fff'; 
    this.buttonColor5 = '#fff'; 
    this.buttonColor6 = '#fff'; 
    this.buttonColor7 = '#fff'; 
    this.buttonColor8 = '#0c4393'; 
    this.fontColor='#0c4393';
    this.fontColor1 = '#0c4393'; 
    this.fontColor2 = '#0c4393'; 
    this.fontColor3 = '#0c4393'; 
    this.fontColor4 = '#0c4393'; 
    this.fontColor5 = '#0c4393'; 
    this.fontColor6 = '#0c4393'; 
    this.fontColor7 = '#0c4393'; 
    this.fontColor8 = '#fff'; 
  this.allservices= [
    {label:'Face &Neck detan Rs.350'},
    {label:'Full arms detan Rs.300'},
    {label:'Full legs detan  Ra.400'},
    {label:'Front detan Rs.350'},
    {label:'Back detan Rs.350'},
    {label:'Full body detan  Rs.1399'},
  ];
 }
  datachanged(e:any,data){
    //console.log(e);
    
    console.log(data);
    if(e.checked==true) {
      console.log("dd")
      this.wasteChecked.push(data);
      console.log(this.wasteChecked);
  }else if(e.checked==false){
    let index = this.wasteChecked.indexOf(data);
      console.log(index + "this is index"+data);
      // if (index == -1) {
        this.wasteChecked.splice(index, 1);
        console.log(this.wasteChecked);
      // }

  }
 
  this.storage.set('regular', this.wasteChecked);
this.length=this.wasteChecked.length
  console.log(this.length)
 
}
spaFinal(){
  this.navCtrl.push('SpaNewPage',{package:this.wasteChecked})
}
}
