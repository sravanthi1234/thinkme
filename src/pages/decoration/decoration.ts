import { DecorationDetailsPage } from './../decoration-details/decoration-details';
import { WeddingCarDetailsPage } from './../wedding-car-details/wedding-car-details';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { ElementRef, NgZone,ViewChild } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { AlertController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular'

import { CourierdetailsPage } from '../../pages/courierdetails/courierdetails';
import {CourierDetailsInfo} from '../../Model/CourierDetailsInfo';
import { MaidDetailsPage } from '../maid-details/maid-details';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl,FormBuilder } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
/**
 * Generated class for the DecorationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-decoration',
  templateUrl: 'decoration.html',
})
export class DecorationPage {
  allservices= [
    {label:'Wedding'},
    {label:'Reception'},
    {label:'Cradle Function'},
    {label:'Saree Function'},  
  
   
  ];
  decoration= [
    {label:'Flower Decoration'},
    {label:'Cloth Decoration'},
    {label:'Other'},
   
  
   
  ];
  @ViewChild("dropAddress")
  public dropAddressElementRef: ElementRef;
  @ViewChild("pickUpAddress")
  public pickUpAddressElementRef: ElementRef;
  
  public zoom: number;
  distance: number = 0;
  fare: number = 0;
  model = new CourierDetailsInfo();
 validation_messages = {
   
  'selected_value': [
    { type: 'required', message: 'Event  is required.' },
  
],
'location': [
  { type: 'required', message: 'please select your location.' },

],
'area': [
  { type: 'required', message: 'please select your location.' },

],
'decoration_value': [
  { type: 'required', message: 'please select your decoration type.' },

],
'budget': [
  { type: 'required', message: 'please select your decoration type.' },

],
'date': [
  { type: 'required', message: 'Date is required' },

],
'time': [
  { type: 'required', message: 'Time is required' },

],



}
  serviceType:any;
  location:any;
  decorType:any;
  budget:any;dateFinal:any;
  selected_value:any;
  decoration_value:any;
  public todo : FormGroup;
  username:any;
  useremail:any;
  area:any;
  time:any;
  phoneNumber:any;
  constructor(private navCtrl:NavController,private geolocation: Geolocation, public mapsAPILoader:MapsAPILoader,private ngZone: NgZone,private formBuilder: FormBuilder,public http: HttpClient, public alertCtrl: AlertController,public actionSheetCtrl: ActionSheetController) {
   

    this.todo = this.formBuilder.group({
     
      selected_value: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      location: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      area: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      decoration_value: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      date: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      budget: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      time: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
    
    
    },
   
    );
  }
  ngOnInit(){
    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
    this.model.fromLat=this.model.fromLat=data.coords.latitude;
    this.model.fromLng=this.model.fromLng=data.coords.longitude;
    var geocoder = new google.maps.Geocoder;
    // this.GetCurrentAddressText(geocoder,this.model.fromLat,this.model.fromLng);
    this.setCurrentPosition();

  });
      this.AutoCompleteMapTextbox();
      
  }



private setCurrentPosition() {
  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.model.fromLat = position.coords.latitude;
      this.model.fromLng = position.coords.longitude;
      this.zoom = 18;
    });
  }
}

async NormalCourier(){
    
    this.presentActionSheet();
}

showAlert() {
  const alert = this.alertCtrl.create({
    subTitle: 'Please Enter Valid Address!',
    buttons: ['OK']
  });
  alert.present();
}


presentActionSheet() {
  let actionSheet = this.actionSheetCtrl.create({
    title: 'Conform Your Address',
    buttons: [
      
      {
        text: 'Drop Address :-' + this.model.toAddress
      },
     
     
      {
        text: 'Conform',
        role: 'conform',cssClass:'btnConfirmClass',
        handler: () => {
          this.gotoCaender();
        }
      }
    ]
  });
  actionSheet.present();
}


gotoCaender(){
 
}

getDistance():Promise<number>{
  return new Promise((resolve,reject)=>{
  var service = new google.maps.DistanceMatrixService();
  service.getDistanceMatrix({
     // origins: [this.model.fromAddress],
      destinations: [this.model.toAddress],
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.METRIC,
      avoidHighways: false,
      avoidTolls: false
  }, function (response, status) {
      if (status == google.maps.DistanceMatrixStatus.OK) {
          var distance = (parseInt(response.rows[0].elements[0].distance.text));
          resolve(distance);
         // var duration = response.rows[0].elements[0].duration.text;

      } else {
          alert("Unable to find the distance via road.");
      }
  });
  
});
}


AutoCompleteMapTextbox(){
  this.mapsAPILoader.load().then(() => {
 
    let autocomplete = new google.maps.places.Autocomplete(this.dropAddressElementRef.nativeElement, {
      types: ["address"]
    });
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();
        this.model.toAddress=place.formatted_address;

        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        this.model.toLat = place.geometry.location.lat();
        this.model.toLag = place.geometry.location.lng();
        this.zoom = 12;
      });
    });
  });


  
  }

  showselected(selected_value)
  {
  console.log("selector: ", selected_value );
  this.serviceType=selected_value;
  console.log(this.serviceType );
  }
  decorationselected(decoration_value)
  {
  console.log("selector: ", decoration_value );
  this.decorType=decoration_value;
  console.log(this.decorType);
  }
  logForm() {
      
    console.log(this.todo.value);
    this.navCtrl.push('DecorationDetailsPage',{category:this.serviceType,category1:this.decorType,category2:this.location,category3:this.budget,area:this.model.toAddress,date:this.dateFinal,time:this.time});
    console.log(this.model.toAddress);
    
  }

  decorationNew(){
    this.navCtrl.push('DecorationDetailsPage',{category:this.serviceType,category1:this.decorType,category2:this.location,category3:this.budget});
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DecorationPage');
  }
  onChange(event) {
    
    console.log(event.format('DD-MM-YYYY')); // For actual usage.
   this.dateFinal=event.format('DD-MM-YYYY');
   console.log(this.dateFinal)
   // console.log(moment(event).format('DD-MM-YYYY')); // the statement you might think about
  }
}
