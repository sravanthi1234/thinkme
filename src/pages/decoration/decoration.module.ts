import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DecorationPage } from './decoration';
import { CalendarModule } from "ion2-calendar";

@NgModule({
  declarations: [
    DecorationPage,
  ],
  imports: [
    IonicPageModule.forChild(DecorationPage),CalendarModule,
  ],
})
export class DecorationPageModule {}
