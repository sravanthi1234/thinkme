import { HomePage } from './../home/home';
import { ThinkmeProvider } from './../../providers/thinkme/thinkme';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,LoadingController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { constructDependencies } from '@angular/core/src/di/reflective_provider';
import { ToastController } from 'ionic-angular';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { AlertController } from 'ionic-angular';


@IonicPage()

@Component({
  selector: 'page-venue-details',
  templateUrl: 'venue-details.html',
})
export class VenueDetailsPage {
  hour:any;
  occation:any;
  service:any;
  religion:any;
  capacity:any;
  area:any;
  gender:any;
  location:any;
  address:any;
  time:any;
  date:any;
  username:any;
  useremail:any;
  phoneNumber:any;
  constructor(public navCtrl: NavController, private alertCtrl: AlertController,public loadingCtrl: LoadingController,public usersService: ThinkmeProvider, public navParams: NavParams,public http: HttpClient,public toastCtrl: ToastController,private storage: Storage) {
    this.occation=this.navParams.get("category");
    this.capacity=this.navParams.get("category1");
    this.location=this.navParams.get("category2");
   this.date=this.navParams.get("category3");
   this.time=this.navParams.get("category4");
    this.address=this.navParams.get("category5");
    console.log(this.location);
    this.storage.get('username').then((val) => {
      console.log('Your name is', val);
      this.username=val;
    });
    this.storage.get('useremail').then((val) => {
      console.log('Your email is', val);
      this.useremail=val;
    });
    this.storage.get('phone').then((val) => {
      console.log('Your phone Number is', val);
      this.phoneNumber=val;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WeddingCarDetailsPage');
  }
  logForm() {
     //console.log(this.todo.value)
    let body ={
      "userid":this.username,
      "eventCategory": "Venue Booking",
      "mainCategory": "Events",
      "subCategory":"Venue Booking",
      "Date":this.date,
      "Time":this.time, 
       "bookingData":
      {
       
         "mainCategory": "Events",
          "subCategory":"Venue Booking",
          "userEmail": this.useremail,
          "phoneNumber":  this.phoneNumber,
          "Date": this.date,
          "Time": this.time,
          "Location" :this.location,
          "Occasion" : this.occation,
          "Capacity": this.capacity,
          "Address":this.address
             }
      }
      
  console.log(body);


  this.usersService.bookingUser(body).subscribe(res => {
    console.log("body here")
    console.log(JSON.stringify(res["Message"]));
    console.log(res);
    let responseMessage = res["Message"];
    if(responseMessage =="We accept your request, After vendor confirmation we will update you"){
      // let toast = this.toastCtrl.create({
      //   message: responseMessage,
      //   duration: 3000,
      //   cssClass: "yourCssClassName",
      //   position: 'bottom'
      // });
      let alert = this.alertCtrl.create({
        title: 'Thank you.!!',
        subTitle: responseMessage,
        buttons: ['ok']
      });
      alert.onDidDismiss(() => {
        // this.loading.dismiss();
        this.navCtrl.setRoot(HomePage);
       });
    //this.loading.present();
    alert.present();
    }else{
    
    }
    })
    



  
  }


}
