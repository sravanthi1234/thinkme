import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VenueDetailsPage } from './venue-details';

@NgModule({
  declarations: [
    VenueDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(VenueDetailsPage),
  ],
})
export class VenueDetailsPageModule {}
