import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeddingCarDetailsPage } from './wedding-car-details';

@NgModule({
  declarations: [
    WeddingCarDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(WeddingCarDetailsPage),
  ],
})
export class WeddingCarDetailsPageModule {}
