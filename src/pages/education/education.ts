import { TutorFinalPage } from './../tutor-final/tutor-final';
import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { ElementRef, NgZone,ViewChild } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { AlertController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular'

import { CourierdetailsPage } from '../../pages/courierdetails/courierdetails';
import {CourierDetailsInfo} from '../../Model/CourierDetailsInfo';
import { MaidDetailsPage } from '../maid-details/maid-details';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl,FormBuilder } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
/**
 * Generated class for the EducationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-education',
  templateUrl: 'education.html',
})
export class EducationPage {
education= [
    {label:'b.tech'},
    {label:'degree'},
    {label:'8th class'},
    {label:'10th class'},  
   
   
  ];
  subject= [
    {label:'Maths'},
    {label:'science'},
    {label:'social'},
    {label:'hindi'},  
   
   
  ];
  time= [
    {label:'7:00'},
    {label:'8:00'},
    {label:'9:00'},
    {label:'10:00'}, 
    {label:'11:00'},  
    {label:'12:00'},  
    {label:'13:00'}, 
    {label:'14:00'}, 
    {label:'15:00'},   
    {label:'16:00'},   
    {label:'17:00'},   
   
   
  ];
  new_selected:any;
  sub_selected:any;
  discripton:any;
  tutor:any;
  days:any;
  public todo : FormGroup;
  validation_messages = {
  
  'date': [
    { type: 'required', message: 'please select your date.' },
  
  ],
 
  'days': [
    { type: 'required', message: 'NUmber of days is required.' },
    { type: 'pattern', message: 'please enter numbers only' }
  
  ],
  'time_selected': [
    { type: 'required', message: 'time is required.' },
  
  ],
  'discripton': [
    { type: 'required', message: 'discription is required.' },
  
  ],
  'sub_selected': [
    { type: 'required', message: 'subject is required.' },
  
  ],
  'new_selected': [
    { type: 'required', message: 'Education  is required.' },
  
  ],
  'tutor': [
    { type: 'required', message: 'tutor type  is required.' },
  
  ],
  'area': [
    { type: 'required', message: 'area  is required.' },
  
  ],
  
  
}

  edu:any;
  sub:any;
  
  date:any;
  timeNew:any;
  area:any;
  discription:any;
  dateFinal:any;
  @ViewChild("dropAddress")
  public dropAddressElementRef: ElementRef;
  @ViewChild("pickUpAddress")
  public pickUpAddressElementRef: ElementRef;
  
  public zoom: number;
  distance: number = 0;
  fare: number = 0;
  model = new CourierDetailsInfo();
  constructor(private navCtrl:NavController,private geolocation: Geolocation, public mapsAPILoader:MapsAPILoader,private ngZone: NgZone,private formBuilder: FormBuilder,public http: HttpClient, public alertCtrl: AlertController,public actionSheetCtrl: ActionSheetController) {
    this.todo = this.formBuilder.group({
     
     
     
      date: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
     
      days: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern("^[1-9][0-9]*")
       
      ])),
      time_selected: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      discripton: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      sub_selected: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      new_selected: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      tutor: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      
      area: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      
    },
    );  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EducationPage');
  }
  ngOnInit(){
    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
    this.model.fromLat=this.model.fromLat=data.coords.latitude;
    this.model.fromLng=this.model.fromLng=data.coords.longitude;
    var geocoder = new google.maps.Geocoder;
    // this.GetCurrentAddressText(geocoder,this.model.fromLat,this.model.fromLng);
    this.setCurrentPosition();

  });
      this.AutoCompleteMapTextbox();
      
  }



private setCurrentPosition() {
  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.model.fromLat = position.coords.latitude;
      this.model.fromLng = position.coords.longitude;
      this.zoom = 18;
    });
  }
}

async NormalCourier(){
    
    this.presentActionSheet();
}

showAlert() {
  const alert = this.alertCtrl.create({
    subTitle: 'Please Enter Valid Address!',
    buttons: ['OK']
  });
  alert.present();
}


presentActionSheet() {
  let actionSheet = this.actionSheetCtrl.create({
    title: 'Conform Your Address',
    buttons: [
      
      {
        text: 'Drop Address :-' + this.model.toAddress
      },
     
     
      {
        text: 'Conform',
        role: 'conform',cssClass:'btnConfirmClass',
        handler: () => {
          this.gotoCaender();
        }
      }
    ]
  });
  actionSheet.present();
}


gotoCaender(){
 
}

getDistance():Promise<number>{
  return new Promise((resolve,reject)=>{
  var service = new google.maps.DistanceMatrixService();
  service.getDistanceMatrix({
     // origins: [this.model.fromAddress],
      destinations: [this.model.toAddress],
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.METRIC,
      avoidHighways: false,
      avoidTolls: false
  }, function (response, status) {
      if (status == google.maps.DistanceMatrixStatus.OK) {
          var distance = (parseInt(response.rows[0].elements[0].distance.text));
          resolve(distance);
         // var duration = response.rows[0].elements[0].duration.text;

      } else {
          alert("Unable to find the distance via road.");
      }
  });
  
});
}


AutoCompleteMapTextbox(){
  this.mapsAPILoader.load().then(() => {
 
    let autocomplete = new google.maps.places.Autocomplete(this.dropAddressElementRef.nativeElement, {
      types: ["address"]
    });
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();
        this.model.toAddress=place.formatted_address;

        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        this.model.toLat = place.geometry.location.lat();
        this.model.toLag = place.geometry.location.lng();
        this.zoom = 12;
      });
    });
  });


  
  }

home(){
  this.navCtrl.push(HomePage)
  console.log("jnskaj")
}
onChange(event) {
    
  console.log(event.format('DD-MM-YYYY')); // For actual usage.
 this.dateFinal=event.format('DD-MM-YYYY');
 console.log(this.dateFinal)
 // console.log(moment(event).format('DD-MM-YYYY')); // the statement you might think about
}
  
  logForm() {
    this.navCtrl.push('TutorFinalPage',{education:this.new_selected,subject:this.sub_selected,finaldate:this.dateFinal,finalTime:this.timeNew,discription:this.discripton,tutorType:this.tutor,noDays:this.days,area:this.model.toAddress})
 console.log(this.edu);
    console.log(this.todo.value);
  }
 
  showselected(new_selected)
  {
  console.log("selector: ", new_selected );
  this.edu=new_selected;
  console.log(this.edu );
  }
  subselected(sub_selected)
  {
  console.log("selector: ", sub_selected );
  this.sub=sub_selected;
  console.log(this.sub );
  }
  timeselected(time_selected)
  {
  console.log("selector: ", time_selected );
  this.timeNew=time_selected;
  console.log(this.timeNew );
  }
}
