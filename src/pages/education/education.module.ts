import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EducationPage } from './education';
import { CalendarModule } from "ion2-calendar";


@NgModule({
  declarations: [
    EducationPage,
  ],
  imports: [
    IonicPageModule.forChild(EducationPage),CalendarModule,
  ],
})
export class EducationPageModule {}
