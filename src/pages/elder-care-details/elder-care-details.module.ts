import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElderCareDetailsPage } from './elder-care-details';

@NgModule({
  declarations: [
    ElderCareDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ElderCareDetailsPage),
  ],
})
export class ElderCareDetailsPageModule {}
