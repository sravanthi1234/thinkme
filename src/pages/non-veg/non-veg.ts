import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FoodTotalPage } from '../food-total/food-total';
import { ActionSheetController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the NonVegPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-non-veg',
  templateUrl: 'non-veg.html',
})
export class NonVegPage {
  allservices= [
    [{label:'Chicken 65', imgUrl:'assets/serviceslogo/ss.jpg',cost:'Rs.290'}],
    [{label:'Chicken Biryani', imgUrl:'assets/serviceslogo/ss.jpg',cost:'Rs.399'}],
    [{label:'Mutton Biryani', imgUrl:'assets/serviceslogo/ss.jpg',cost:'Rs.199'}],
  [{label:'Fish Biryani', imgUrl:'assets/serviceslogo/ss.jpg',cost:'Rs.290'}],
   ];
   wasteChecked: any = [];
  key: any;
  constructor(public navCtrl: NavController,private storage: Storage,public actionSheetCtrl: ActionSheetController, public navParams: NavParams) {
    this.navParams.get("category");
    
    this.storage.get(this.key).then((val) => {
      console.log('Your age is', val);
    });
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NonVegPage');
   
  }
   
  
  foodTotal(){
  
    this. presentActionSheet()
  }
  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Price',
      buttons: [
        {
          text: 'Your  Total Cost is :-' + " Rs.1500"
        },
        
      
      
        
        {
          text: 'Confirm',
          role: 'conform',cssClass:'btnConfirmClass',
          handler: () => {
            this.gotototal();
          }
        }
      ]
    });
    actionSheet.present();
  }
  gotototal(){
    this.navCtrl.push(FoodTotalPage, {categorynon:this.wasteChecked});
   
  }
  datachanged(e:any,data){
    //console.log(e);
    console.log(data);
    if(e.checked==true) {
      console.log("dd")
      this.wasteChecked.push(data);
      console.log(this.wasteChecked);
  }else if(e.checked==false){
    let index = this.wasteChecked.indexOf(data);
      console.log(index + "this is index"+data);
      // if (index == -1) {
        this.wasteChecked.splice(index, 1);
        console.log(this.wasteChecked);
      // }

  }
  this.storage.set('nonveg', this.wasteChecked);
}
}
