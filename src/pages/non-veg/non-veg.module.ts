import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NonVegPage } from './non-veg';

@NgModule({
  declarations: [
    NonVegPage,
  ],
  imports: [
    IonicPageModule.forChild(NonVegPage),
  ],
})
export class NonVegPageModule {}
