import { ThinkmeProvider } from './../../providers/thinkme/thinkme';
import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { constructDependencies } from '@angular/core/src/di/reflective_provider';

import { ToastController } from 'ionic-angular';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the MakeupDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-makeup-details',
  templateUrl: 'makeup-details.html',
})
export class MakeupDetailsPage {
  dateFinal:any;
  budget:any;
  bridalnew:any;
  username:any;
  useremail:any;
  time:any;
  area:any;
  phoneNumber:any;
  constructor(public navCtrl: NavController, private alertCtrl: AlertController,public loadingCtrl: LoadingController,public usersService: ThinkmeProvider, public navParams: NavParams,public http: HttpClient,public toastCtrl: ToastController,private storage: Storage) {
    this.dateFinal=this.navParams.get("category");
    this.bridalnew=this.navParams.get("category2");
    this.time=this.navParams.get("category3");
    this.area=this.navParams.get("category4");
    console.log(this.dateFinal);
  
    console.log(this.bridalnew);
    this.storage.get('username').then((val) => {
      console.log('Your name is', val);
      this.username=val;
    });
    this.storage.get('useremail').then((val) => {
      console.log('Your email is', val);
      this.useremail=val;
    });
    this.storage.get('phone').then((val) => {
      console.log('Your phone Number is', val);
      this.phoneNumber=val;
    });
  
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MakeupDetailsPage');
  }
  logForm() {
      
    //console.log(this.todo.value)
    let body ={
      "userid":this.username,
      "eventCategory": "Makeup",
      "mainCategory": "Events",
      "subCategory":"Makeup",  
      "Date":this.dateFinal,
      "Time":this.time,   
      "bookingData":
      {
 "mainCategory": "Events",
"subCategory":"Makeup",
"userEmail": this.useremail,
"phoneNumber":this.phoneNumber,
"Date":this.dateFinal,
"Time":this.time,
"Area":this.area,
"Makeuptype":this.bridalnew

      }
      }
      
  console.log(body);


this.usersService.bookingUser(body).subscribe(res => {
console.log(JSON.stringify(res["Message"]));
console.log(res);
let responseMessage = res["Message"];
if(responseMessage =="We accept your request, After vendor confirmation we will update you"){
  // let toast = this.toastCtrl.create({
  //   message: responseMessage,
  //   duration: 3000,
  //   cssClass: "yourCssClassName",
  //   position: 'bottom'
  // });
  let alert = this.alertCtrl.create({
    title: 'Thank you.!!',
    subTitle: responseMessage,
    buttons: ['ok']
  });
  alert.onDidDismiss(() => {
    // this.loading.dismiss();
    this.navCtrl.setRoot(HomePage);
   });
//this.loading.present();
alert.present();
}else{

}
})



  
  }

}
