import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MakeupDetailsPage } from './makeup-details';

@NgModule({
  declarations: [
    MakeupDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(MakeupDetailsPage),
  ],
})
export class MakeupDetailsPageModule {}
