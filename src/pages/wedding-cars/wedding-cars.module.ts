import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeddingCarsPage } from './wedding-cars';
import { CalendarModule } from "ion2-calendar";

@NgModule({
  declarations: [
    WeddingCarsPage,
  ],
  imports: [
    IonicPageModule.forChild(WeddingCarsPage),CalendarModule,
  ],
})
export class WeddingCarsPageModule {}
