import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlannersPage } from './planners';
import { CalendarModule } from "ion2-calendar";


@NgModule({
  declarations: [
    PlannersPage,
  ],
  imports: [
    IonicPageModule.forChild(PlannersPage),CalendarModule,
  ],
})
export class PlannersPageModule {}
