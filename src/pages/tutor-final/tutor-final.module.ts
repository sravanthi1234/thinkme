import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TutorFinalPage } from './tutor-final';

@NgModule({
  declarations: [
    TutorFinalPage,
  ],
  imports: [
    IonicPageModule.forChild(TutorFinalPage),
  ],
})
export class TutorFinalPageModule {}
