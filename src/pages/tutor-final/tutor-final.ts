import { HomePage } from './../home/home';
import { LoginPage } from './../login/login';
import { ThinkmeProvider } from './../../providers/thinkme/thinkme';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the TutorFinalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tutor-final',
  templateUrl: 'tutor-final.html',
})
export class TutorFinalPage {
  childEdu:any;
  childSub:any;
  childDate:any;
  childFinal:any;
  booking:any=[];
  tutor:any;
  days:any;
  discription:any;
  username:any;
  useremail:any;
  area:any;
  phoneNumber:any;
  loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  constructor(public navCtrl: NavController, private alertCtrl: AlertController,public loadingCtrl: LoadingController,public usersService: ThinkmeProvider, public navParams: NavParams,public http: HttpClient,public toastCtrl: ToastController,private storage: Storage) {
  this.childEdu=  this.navParams.get("education");
  console.log(this.childEdu);
  this.childSub=  this.navParams.get("subject");
  console.log(this.childSub);
  this.childDate=  this.navParams.get("finaldate");
  console.log(this.childDate);
  this.childFinal=  this.navParams.get("finalTime");
  console.log(this.childFinal);
  this.discription=  this.navParams.get("discription");
  console.log(this.discription);
  this.tutor=  this.navParams.get("tutorType");
  console.log(this.tutor);
  this.days=  this.navParams.get("noDays");
  console.log(this.days);
  this.area=  this.navParams.get("area");
  console.log(this.area);
  this.storage.get('username').then((val) => {
    console.log('Your name is', val);
    this.username=val;
  });
  this.storage.get('useremail').then((val) => {
    console.log('Your email is', val);
    this.useremail=val;
  });
  this.storage.get('phone').then((val) => {
    console.log('Your phone Number is', val);
    this.phoneNumber=val;
  });

  
 
  console.log(this.booking);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TutorFinalPage');
  }
  logForm() {
      
    //console.log(this.todo.value)
    let body = {
      "userid":this.username,
"eventCategory": "Education",
"mainCategory": "Tutors",
  "subCategory":"Education",
  "Date": this.childDate,
"Time": this.childFinal,
"bookingData":{
  "mainCategory": "Events",
  "subCategory":"Education",
  "userEmail": this.useremail,
  "phoneNumber":  this.phoneNumber,
  "Area":this.area,
  "Education_type": this.tutor,
  "Education":this.childEdu,
  "Subject":this.childSub,
  "No_day_week":parseInt(this.days),
  "Time":this.childFinal,
  "Date":this.childDate,
  "Description":this.discription
}
  }
  console.log(body);


this.usersService.bookingUser(body).subscribe(res => {
console.log(JSON.stringify(res["Message"]));
console.log(res);
let responseMessage = res["Message"];
if(responseMessage =="We accept your request, After vendor confirmation we will update you"){
  // let toast = this.toastCtrl.create({
  //   message: responseMessage,
  //   duration: 3000,
  //   cssClass: "yourCssClassName",
  //   position: 'bottom'
  // });
  let alert = this.alertCtrl.create({
    title: 'Thank you.!!',
    subTitle: responseMessage,
    buttons: ['ok']
  });
  alert.onDidDismiss(() => {
    // this.loading.dismiss();
    this.navCtrl.setRoot(HomePage);
   });
//this.loading.present();
alert.present();
}else{

}
})



  
  }

}
