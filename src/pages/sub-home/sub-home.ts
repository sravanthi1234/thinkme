import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegisterPage } from '../register/register';

/**
 * Generated class for the SubHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sub-home',
  templateUrl: 'sub-home.html',
})
export class SubHomePage {
  type:any;
  public buttonClickedNew: boolean = true;
  public buttonClicked: boolean = false; //Whatever you want to initialise it as
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubHomePage');
  }
  subHome(){
    this.buttonClicked = !this.buttonClicked;
    this.buttonClickedNew = !this.buttonClickedNew;
  
  }
  register(){
    this.navCtrl.push(RegisterPage)
  }
}
