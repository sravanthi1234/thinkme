import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhotographyDetailsPage } from './photography-details';

@NgModule({
  declarations: [
    PhotographyDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PhotographyDetailsPage),
  ],
})
export class PhotographyDetailsPageModule {}
