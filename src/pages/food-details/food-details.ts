import { VegPage } from './../veg/veg';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NonVegPage } from '../non-veg/non-veg';
import { Storage } from '@ionic/storage';
import { FoodTotalPage } from '../food-total/food-total';

/**
 * Generated class for the FoodDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-food-details',
  templateUrl: 'food-details.html',
})
export class FoodDetailsPage {
  tab1Root = VegPage;
  tab2Root = NonVegPage;
  veggie:any=[];
  vegNew:any=[];
  nvnew:any=[];

nonveggie:any=[];
 
  constructor(public navCtrl: NavController,private storage: Storage, public navParams: NavParams) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoodDetailsPage');
  }

  foodTotal(){
    
    this.navCtrl.push(FoodTotalPage);
    
    
  }
}
