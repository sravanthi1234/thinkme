import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WpDetailsPage } from './wp-details';

@NgModule({
  declarations: [
    WpDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(WpDetailsPage),
  ],
})
export class WpDetailsPageModule {}
