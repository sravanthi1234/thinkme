import { HomePage } from './../home/home';
import { ThinkmeProvider } from './../../providers/thinkme/thinkme';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { constructDependencies } from '@angular/core/src/di/reflective_provider';
import { ToastController } from 'ionic-angular';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { AlertController } from 'ionic-angular';
/**
 * Generated class for the MehandiDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()

@Component({
  selector: 'page-wp-details',
  templateUrl: 'wp-details.html',
})
export class WpDetailsPage {
  hour:any;
  service:any;
  religion:any;
  area:any;
  gender:any;
  location:any;
  date:any;
  username:any;
  useremail:any;
  phoneNumber:any;
  time:any;
  constructor(public navCtrl: NavController, private alertCtrl: AlertController,public loadingCtrl: LoadingController,public usersService: ThinkmeProvider, public navParams: NavParams,public http: HttpClient,public toastCtrl: ToastController,private storage: Storage) {
    this.hour=this.navParams.get("category");
    this.service=this.navParams.get("category1");
    this.religion=this.navParams.get("category2");
   this.date=this.navParams.get("category3");
    this.gender=this.navParams.get("category4");
    this.location=this.navParams.get("category5");
    console.log(this.location);
    this.time=this.navParams.get("time");
    this.storage.get('username').then((val) => {
      console.log('Your name is', val);
      this.username=val;
    });
    this.storage.get('useremail').then((val) => {
      console.log('Your email is', val);
      this.useremail=val;
    });
    this.storage.get('phone').then((val) => {
      console.log('Your phone Number is', val);
      this.phoneNumber=val;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MehandiDetailsPage');
  }
  logForm(){
      
    //console.log(this.todo.value)
    let body ={
      "userid":this.username,
      "eventCategory": "Wedding Planners",
      "mainCategory": "Events",
      "subCategory":"Wedding Planners",
      "Date":this.date,
      "Time":this.time, 
       "bookingData":
      {
        "userid":this.username,
        "mainCategory": "Events",
        "subCategory":"Wedding Planners",
        "userEmail": this.useremail,
     "phoneNumber":  this.phoneNumber,
     "Time":this.time,
        "Date": this.date,
        "Location" :this.location,
        "Event" : this.hour
        
      }
      }
      
  console.log(body);


this.usersService.bookingUser(body).subscribe(res => {
console.log(JSON.stringify(res["Message"]));
console.log(res);
let responseMessage = res["Message"];
if(responseMessage =="We accept your request, After vendor confirmation we will update you"){
  // let toast = this.toastCtrl.create({
  //   message: responseMessage,
  //   duration: 3000,
  //   cssClass: "yourCssClassName",
  //   position: 'bottom'
  // });
  let alert = this.alertCtrl.create({
    title: 'Thank you.!!',
    subTitle: responseMessage,
    buttons: ['ok']
  });
  alert.onDidDismiss(() => {
    // this.loading.dismiss();
    this.navCtrl.setRoot(HomePage);
   });
//this.loading.present();
alert.present();
}else{

}
})



  
  }

}
