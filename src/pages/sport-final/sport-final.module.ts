import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SportFinalPage } from './sport-final';

@NgModule({
  declarations: [
    SportFinalPage,
  ],
  imports: [
    IonicPageModule.forChild(SportFinalPage),
  ],
})
export class SportFinalPageModule {}
