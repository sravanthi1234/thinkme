import { HomePage } from './../home/home';
import { ThinkmeProvider } from './../../providers/thinkme/thinkme';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the SportFinalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sport-final',
  templateUrl: 'sport-final.html',
})
export class SportFinalPage {
date:any;
tennis:any;
sportstime:any;
childage:any;
days:any;
booking:any=[];
discripton:any;
tutor:any;
username:any;
useremail:any;
area:any;
phoneNumber:any;
  constructor(public navCtrl: NavController, private alertCtrl: AlertController,public loadingCtrl: LoadingController,public usersService: ThinkmeProvider, public navParams: NavParams,public http: HttpClient,public toastCtrl: ToastController,private storage: Storage) {
    this.date=this.navParams.get("sportDate");
    console.log(this.date);
    this.tennis=this.navParams.get("sportType");
    console.log(this.tennis);
    this.sportstime=this.navParams.get("sportTime");
    console.log(this.sportstime);

    this.childage=this.navParams.get("childAge");
    console.log(this.childage);
this.days=this.navParams.get("days");
this.discripton=this.navParams.get("discripton");
this.tutor=this.navParams.get("tutor");
this.area=this.navParams.get("area");
this.storage.get('username').then((val) => {
  console.log('Your name is', val);
  this.username=val;
});
this.storage.get('useremail').then((val) => {
  console.log('Your email is', val);
  this.useremail=val;
});
this.storage.get('phone').then((val) => {
  console.log('Your phone Number is', val);
  this.phoneNumber=val;
});
    
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SportFinalPage');
  }
  logForm() {
      
    //console.log(this.todo.value)
    let body ={
      "userid":this.username,
      "eventCategory": "Sports",
      "mainCategory": "Tutors",
  "subCategory":"Sports",
  "Date":this.date,
      "Time":this.sportstime, 
      "bookingData":
      {
       "mainCategory": "tutors",
      "subCategory":"Sports",
      "userEmail": this.useremail,
      "phoneNumber":  this.phoneNumber,
      "Area":this.area,
      "Sport_type": this.tutor,
      "Child_age":parseInt(this.childage),
      "Sport": this.tennis,
      "Date": this.date,
      "Time": this.sportstime,
      "Num_day" :this.days,
      "Description" : this.discripton
      }
      }
      
  console.log(body);


this.usersService.bookingUser(body).subscribe(res => {
console.log(JSON.stringify(res["Message"]));
console.log(res);
let responseMessage = res["Message"];
if(responseMessage =="We accept your request, After vendor confirmation we will update you"){
  // let toast = this.toastCtrl.create({
  //   message: responseMessage,
  //   duration: 3000,
  //   cssClass: "yourCssClassName",
  //   position: 'bottom'
  // });
  let alert = this.alertCtrl.create({
    title: 'Thank you.!!',
    subTitle: responseMessage,
    buttons: ['ok']
  });
  alert.onDidDismiss(() => {
    // this.loading.dismiss();
    this.navCtrl.setRoot(HomePage);
   });
//this.loading.present();
alert.present();
}else{

}
})



  
  }
}
