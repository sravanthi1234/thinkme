import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VenueBookingPage } from './venue-booking';
import { CalendarModule } from "ion2-calendar";



@NgModule({
  declarations: [
    VenueBookingPage,
  ],
  imports: [
    IonicPageModule.forChild(VenueBookingPage),CalendarModule,
  ],
})
export class VenueBookingPageModule {}
