import { VenueDetailsPage } from './../venue-details/venue-details';
import { WeddingCarDetailsPage } from './../wedding-car-details/wedding-car-details';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { ElementRef, NgZone,ViewChild } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { AlertController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular'

import { CourierdetailsPage } from '../../pages/courierdetails/courierdetails';
import {CourierDetailsInfo} from '../../Model/CourierDetailsInfo';
import { MaidDetailsPage } from '../maid-details/maid-details';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl,FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders  } from '@angular/common/http';


@IonicPage()

@Component({
  selector: 'page-venue-booking',
  templateUrl: 'venue-booking.html',
})
export class VenueBookingPage {

  hour:any;
  Religion:any;
  area:any;
  service:any;
  Gender:any;
  time:any;
  place:any;
  dateFinal:any;
  hours= [
    {label:'Wedding'},
    {label:'Party'},
    {label:'Reception'},
   
  ];
  services= [
    {label:'20 members'},
    {label:'50 members'},
    {label:'70 members'},
    {label:'100 members'},
  ];
  public todo : FormGroup;
  validation_messages = {
   
   
  'location': [
    { type: 'required', message: 'please select your location.' },
  
  ],
  'date': [
    { type: 'required', message: 'please select your date.' },
  
  ],
  'place': [
    { type: 'required', message: 'please select your place.' },
  
  ],
  'time': [
    { type: 'required', message: 'please select your time.' },
  
  ],
  'new_selected': [
    { type: 'required', message: 'please select your event.' },
  
  ],
  'service_selected': [
    { type: 'required', message: 'please select your Capacity.' },
  
  ],
  
  
  
  }
  @ViewChild("dropAddress")
  public dropAddressElementRef: ElementRef;
  @ViewChild("pickUpAddress")
  public pickUpAddressElementRef: ElementRef;
  
  public zoom: number;
  distance: number = 0;
  fare: number = 0;
  model = new CourierDetailsInfo();

  constructor(private navCtrl:NavController,private formBuilder: FormBuilder,public http: HttpClient,private geolocation: Geolocation, public mapsAPILoader:MapsAPILoader,private ngZone: NgZone, public alertCtrl: AlertController,public actionSheetCtrl: ActionSheetController) {
    this.todo = this.formBuilder.group({
     
     
      location: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      date: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      place: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      time: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      new_selected: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      service_selected: new FormControl('', Validators.compose([
        Validators.required
       
      ])),

      
    },
    ); 
  }

  ngOnInit(){

    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
    this.model.fromLat=this.model.fromLat=data.coords.latitude;
    this.model.fromLng=this.model.fromLng=data.coords.longitude;
    var geocoder = new google.maps.Geocoder;
    // this.GetCurrentAddressText(geocoder,this.model.fromLat,this.model.fromLng);
    this.setCurrentPosition();

  });
      this.AutoCompleteMapTextbox();
  }



private setCurrentPosition() {
  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.model.fromLat = position.coords.latitude;
      this.model.fromLng = position.coords.longitude;
      this.zoom = 18;
    });
  }
}

async NormalCourier(){
    
    this.presentActionSheet();
}

showAlert() {
  const alert = this.alertCtrl.create({
    subTitle: 'Please Enter Valid Address!',
    buttons: ['OK']
  });
  alert.present();
}


presentActionSheet() {
  let actionSheet = this.actionSheetCtrl.create({
    title: 'Conform Your Address',
    buttons: [
      
      {
        text: 'Drop Address :-' + this.model.toAddress
      },
     
     
      {
        text: 'Conform',
        role: 'conform',cssClass:'btnConfirmClass',
        handler: () => {
          this.gotoCaender();
        }
      }
    ]
  });
  actionSheet.present();
}


gotoCaender(){
  this.navCtrl.push('VenueDetailsPage',{category:this.hour,category1:this.service,category2:this.place,category3:this.dateFinal,category4:this.time,category5:this.model.toAddress});
console.log(this.model.toAddress);
}

getDistance():Promise<number>{
  return new Promise((resolve,reject)=>{
  var service = new google.maps.DistanceMatrixService();
  service.getDistanceMatrix({
     // origins: [this.model.fromAddress],
      destinations: [this.model.toAddress],
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.METRIC,
      avoidHighways: false,
      avoidTolls: false
  }, function (response, status) {
      if (status == google.maps.DistanceMatrixStatus.OK) {
          var distance = (parseInt(response.rows[0].elements[0].distance.text));
          resolve(distance);
         // var duration = response.rows[0].elements[0].duration.text;

      } else {
          alert("Unable to find the distance via road.");
      }
  });
  
});
}


AutoCompleteMapTextbox(){
  this.mapsAPILoader.load().then(() => {
 
    let autocomplete = new google.maps.places.Autocomplete(this.dropAddressElementRef.nativeElement, {
      types: ["address"]
    });
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();
        this.model.toAddress=place.formatted_address;

        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        this.model.toLat = place.geometry.location.lat();
        this.model.toLag = place.geometry.location.lng();
        this.zoom = 12;
      });
    });
  });


  
  }

  logChange(event){
    console.log(event);
  }
  logForm() {
    this.navCtrl.push('VenueDetailsPage',{category:this.hour,category1:this.service,category2:this.place,category3:this.dateFinal,category4:this.time,category5:this.model.toAddress});
    console.log(this.model.toAddress);
    console.log(this.todo.value)
  }
  onChange(event) {
    
    console.log(event.format('DD-MM-YYYY')); // For actual usage.
   this.dateFinal=event.format('DD-MM-YYYY');
   console.log(this.dateFinal)
   // console.log(moment(event).format('DD-MM-YYYY')); // the statement you might think about
  }
  showselected(new_selected)
  {
  console.log("selector: ", new_selected );
  this.hour=new_selected;
  console.log(this.hour);
  }
  serviceselected(service_selected)
  {
  console.log("selector: ", service_selected );
  this.service=service_selected;
  console.log(this.service);
  }
}
