import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SportsPage } from './sports';
import { CalendarModule } from "ion2-calendar";


@NgModule({
  declarations: [
    SportsPage,
  ],
  imports: [
    IonicPageModule.forChild(SportsPage),CalendarModule,
  ],
})
export class SportsPageModule {}
