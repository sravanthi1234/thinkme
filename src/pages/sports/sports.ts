import { SportFinalPage } from './../sport-final/sport-final';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { ElementRef, NgZone,ViewChild } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { AlertController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular'

import { CourierdetailsPage } from '../../pages/courierdetails/courierdetails';
import {CourierDetailsInfo} from '../../Model/CourierDetailsInfo';
import { MaidDetailsPage } from '../maid-details/maid-details';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl,FormBuilder } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { HttpClient, HttpHeaders  } from '@angular/common/http';

/**
 * Generated class for the SportsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sports',
  templateUrl: 'sports.html',
})
export class SportsPage {
tutor:any;
age:any;
sport:any;
days:any;
timeNew:any;
date:any;
area:any;
discripton:any;
dateSport:any;
sports= [
  {label:'Badminton'},
  {label:'Cricket'},
  {label:'Volley ball'},
 
 
 
];
time= [
  {label:'7:00'},
  {label:'8:00'},
  {label:'9:00'},
  {label:'10:00'}, 
  {label:'11:00'},  
  {label:'12:00'},  
  {label:'13:00'}, 
  {label:'14:00'}, 
  {label:'15:00'},   
  {label:'16:00'},   
  {label:'17:00'},   
 
 
];
@ViewChild("dropAddress")
  public dropAddressElementRef: ElementRef;
  @ViewChild("pickUpAddress")
  public pickUpAddressElementRef: ElementRef;
  
  public zoom: number;
  distance: number = 0;
  fare: number = 0;
  model = new CourierDetailsInfo();
public todo : FormGroup;
  validation_messages = {
  
  'date': [
    { type: 'required', message: 'please select your date.' },
  
  ],
  'age': [
    { type: 'required', message: 'Age is required' },
    { type: 'pattern', message: 'please enter numbers only' }
  
  ],
  
  'new_selected': [
    { type: 'required', message: 'Sport is required.' },
  
  ],
  'time_selected': [
    { type: 'required', message: 'time is required.' },
  
  ],
  'discripton': [
    { type: 'required', message: 'discription is required.' },
  
  ],
  'sub_selected': [
    { type: 'required', message: 'subject is required.' },
  
  ],
  'days': [
    { type: 'required', message: 'Age is required' },
    { type: 'pattern', message: 'please enter numbers only' }
  
  ],
  
  
  'tutor': [
    { type: 'required', message: 'tutor type  is required.' },
  
  ],
  'area': [
    { type: 'required', message: 'area  is required.' },
  
  ],
  
  
}

  constructor(private navCtrl:NavController,private geolocation: Geolocation, public mapsAPILoader:MapsAPILoader,private ngZone: NgZone,private formBuilder: FormBuilder,public http: HttpClient, public alertCtrl: AlertController,public actionSheetCtrl: ActionSheetController) {
    this.todo = this.formBuilder.group({
     
      tutor: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      
      age: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern("^[1-9][0-9]*")
       
      ])),
     
      new_selected: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      date: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
     
      days: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern("^[1-9][0-9]*")
       
      ])),
      time_selected: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      discripton: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      area: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
    
     
    
      
      
    },
    );  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SportsPage');
  }
  ngOnInit(){
    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
    this.model.fromLat=this.model.fromLat=data.coords.latitude;
    this.model.fromLng=this.model.fromLng=data.coords.longitude;
    var geocoder = new google.maps.Geocoder;
    // this.GetCurrentAddressText(geocoder,this.model.fromLat,this.model.fromLng);
    this.setCurrentPosition();

  });
      this.AutoCompleteMapTextbox();
      
  }



private setCurrentPosition() {
  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.model.fromLat = position.coords.latitude;
      this.model.fromLng = position.coords.longitude;
      this.zoom = 18;
    });
  }
}

async NormalCourier(){
    
    this.presentActionSheet();
}

showAlert() {
  const alert = this.alertCtrl.create({
    subTitle: 'Please Enter Valid Address!',
    buttons: ['OK']
  });
  alert.present();
}


presentActionSheet() {
  let actionSheet = this.actionSheetCtrl.create({
    title: 'Conform Your Address',
    buttons: [
      
      {
        text: 'Drop Address :-' + this.model.toAddress
      },
     
     
      {
        text: 'Conform',
        role: 'conform',cssClass:'btnConfirmClass',
        handler: () => {
          this.gotoCaender();
        }
      }
    ]
  });
  actionSheet.present();
}


gotoCaender(){
 
}

getDistance():Promise<number>{
  return new Promise((resolve,reject)=>{
  var service = new google.maps.DistanceMatrixService();
  service.getDistanceMatrix({
     // origins: [this.model.fromAddress],
      destinations: [this.model.toAddress],
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.METRIC,
      avoidHighways: false,
      avoidTolls: false
  }, function (response, status) {
      if (status == google.maps.DistanceMatrixStatus.OK) {
          var distance = (parseInt(response.rows[0].elements[0].distance.text));
          resolve(distance);
         // var duration = response.rows[0].elements[0].duration.text;

      } else {
          alert("Unable to find the distance via road.");
      }
  });
  
});
}


AutoCompleteMapTextbox(){
  this.mapsAPILoader.load().then(() => {
 
    let autocomplete = new google.maps.places.Autocomplete(this.dropAddressElementRef.nativeElement, {
      types: ["address"]
    });
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();
        this.model.toAddress=place.formatted_address;

        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        this.model.toLat = place.geometry.location.lat();
        this.model.toLag = place.geometry.location.lng();
        this.zoom = 12;
      });
    });
  });


  
  }

  onChange(event) {
    
    console.log(event.format('DD-MM-YYYY')); // For actual usage.
   this.dateSport=event.format('DD-MM-YYYY');
   console.log(this.dateSport)
   // console.log(moment(event).format('DD-MM-YYYY')); // the statement you might think about
  }
  showselected(new_selected)
  {
  console.log("selector: ", new_selected );
  this.sport=new_selected;
  console.log(this.sport);
  }
  logForm() {
    this.navCtrl.push('SportFinalPage',{sportDate:this.dateSport,sportType:this.sport,sportTime:this.timeNew,childAge:this.age,days:this.days,tutor:this.tutor,area:this.model.toAddress})
 
    console.log(this.todo.value);
  }
  timeselected(time_selected)
  {
  console.log("selector: ", time_selected );
  this.timeNew=time_selected;
  console.log(this.timeNew );
  }
}
