import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpaListPage } from './spa-list';

@NgModule({
  declarations: [
    SpaListPage,
  ],
  imports: [
    IonicPageModule.forChild(SpaListPage),
  ],
})
export class SpaListPageModule {}
