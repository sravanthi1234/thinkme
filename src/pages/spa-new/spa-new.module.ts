import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpaNewPage } from './spa-new';
import { CalendarModule } from "ion2-calendar";



@NgModule({
  declarations: [
    SpaNewPage,
  ],
  imports: [
    IonicPageModule.forChild(SpaNewPage),CalendarModule,
  ],
})
export class SpaNewPageModule {}
