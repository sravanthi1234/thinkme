import { SpaFinalPage } from './../spa-final/spa-final';
import { WpDetailsPage } from './../wp-details/wp-details';
import { MehandiDetailsPage } from './../mehandi-details/mehandi-details';

//import { PlannersPage } from './planners';

import { PhotographyDetailsPage } from './../photography-details/photography-details';
import { WeddingCarDetailsPage } from './../wedding-car-details/wedding-car-details';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { ElementRef, NgZone,ViewChild } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { AlertController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular'

import { CourierdetailsPage } from '../../pages/courierdetails/courierdetails';
import {CourierDetailsInfo} from '../../Model/CourierDetailsInfo';
import { MaidDetailsPage } from '../maid-details/maid-details';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl,FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import * as moment from 'moment';


@IonicPage()
@Component({
  selector: 'page-spa-New',
  templateUrl: 'spa-New.html',
})
export class SpaNewPage {

  hour:any;
  Religion:any;
  area:any;
  service:any;
  Gender:any;
  dateFinal:any;
  budget:any;
  time:any;
  packges:any=[];
  lengthnew:any;
  hours= [
    {label:'Wedding'},
    {label:'Pre wedding'},
    {label:'Engagement'}, 
    {label:'Sangeet'},
    {label:'Cradle ceremony'},
    {label:'Cocktail Party'},
    {label:'Others'},
  ];
  services= [
    {label:'Mercedes-Benz'},
    {label:'Audi A3 Cabriolet'},
    {label:'Range Rover'},
    {label:'Swift Desire'},
    
  ];
  public todo : FormGroup;
  validation_messages = {
   
   
  'location': [
    { type: 'required', message: 'please select your location.' },
  
  ],
  'date': [
    { type: 'required', message: 'please select your date.' },
  
  ],
 
  'time': [
    { type: 'required', message: 'please select your event.' },
  
  ],
 
}
  @ViewChild("dropAddress")
  public dropAddressElementRef: ElementRef;
  @ViewChild("pickUpAddress")
  public pickUpAddressElementRef: ElementRef;
  
  public zoom: number;
  distance: number = 0;
  fare: number = 0;
  model = new CourierDetailsInfo();
  day: string;
  Date: any;
  Month: any;
  year: string;

  constructor(private navCtrl:NavController,private formBuilder: FormBuilder,public http: HttpClient,private geolocation: Geolocation, public mapsAPILoader:MapsAPILoader,private ngZone: NgZone, public alertCtrl: AlertController,public actionSheetCtrl: ActionSheetController,public navParams: NavParams) {
  this.packges=  this.navParams.get("package");
  this.day=moment().format('dddd');
  this.Date=moment().format('DD');
  this.Month=moment().format('MMM');
  this.year=moment().format('YYYY');
  console.log(this.packges);
  if(this.packges.length==0){

  }
  else{
    this.lengthnew=this.packges.length;
  }
  
  console.log("length is",this.lengthnew);
    this.todo = this.formBuilder.group({
     
     
    
      date: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
     
      time: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
     
     

      
    },
    ); 
  }

  ngOnInit(){
    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
    this.model.fromLat=this.model.fromLat=data.coords.latitude;
    this.model.fromLng=this.model.fromLng=data.coords.longitude;
    var geocoder = new google.maps.Geocoder;
    // this.GetCurrentAddressText(geocoder,this.model.fromLat,this.model.fromLng);
    this.setCurrentPosition();

  });
      this.AutoCompleteMapTextbox();
  }



private setCurrentPosition() {
  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.model.fromLat = position.coords.latitude;
      this.model.fromLng = position.coords.longitude;
      this.zoom = 18;
    });
  }
}

async NormalCourier(){
    
    this.presentActionSheet();
}

showAlert() {
  const alert = this.alertCtrl.create({
    subTitle: 'Please Enter Valid Address!',
    buttons: ['OK']
  });
  alert.present();
}


presentActionSheet() {
  let actionSheet = this.actionSheetCtrl.create({
    title: 'Conform Your Address',
    buttons: [
      
      {
        text: 'Drop Address :-' + this.model.toAddress
      },
     
     
      {
        text: 'Conform',
        role: 'conform',cssClass:'btnConfirmClass',
        handler: () => {
          this.gotoCaender();
        }
      }
    ]
  });
  actionSheet.present();
}


gotoCaender(){
  this.navCtrl.push('WpDetailsPage',{category:this.hour,category1:this.budget,category2:this.Religion,category3:this.dateFinal,category4:this.Gender,category5:this.model.toAddress});
console.log(this.model.toAddress);
}

getDistance():Promise<number>{
  return new Promise((resolve,reject)=>{
  var service = new google.maps.DistanceMatrixService();
  service.getDistanceMatrix({
     // origins: [this.model.fromAddress],
      destinations: [this.model.toAddress],
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.METRIC,
      avoidHighways: false,
      avoidTolls: false
  }, function (response, status) {
      if (status == google.maps.DistanceMatrixStatus.OK) {
          var distance = (parseInt(response.rows[0].elements[0].distance.text));
          resolve(distance);
         // var duration = response.rows[0].elements[0].duration.text;

      } else {
          alert("Unable to find the distance via road.");
      }
  });
  
});
}


AutoCompleteMapTextbox(){
  this.mapsAPILoader.load().then(() => {
 
    let autocomplete = new google.maps.places.Autocomplete(this.dropAddressElementRef.nativeElement, {
      types: ["address"]
    });
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();
        this.model.toAddress=place.formatted_address;

        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        this.model.toLat = place.geometry.location.lat();
        this.model.toLag = place.geometry.location.lng();
        this.zoom = 12;
      });
    });
  });


  
  }

  logChange(event){
    console.log(event);
  }
  logForm() {
    this.navCtrl.push('SpaListPage',{category:this.time,category1:this.packges,category3:this.dateFinal,category5:this.model.toAddress});
    console.log(this.model.toAddress);
    console.log(this.todo.value)
  }
  onChange(event) {
    
    console.log(event.format('DD-MM-YYYY')); // For actual usage.
   this.dateFinal=event.format('DD-MM-YYYY');
   console.log(this.dateFinal);
   console.log(event);
     // the statement you might think about
    var day = moment(this.dateFinal);
    console.log(day);
    console.log(moment(event).format('dddd'));
    this.day=moment(event).format('dddd');
    this.Date=moment(event).format('DD');
    this.Month=moment(event).format('MMM');
    this.year=moment(event).format('YYYY');
    console.log(this.day,this.Date,this.Month,this.year)
  }
  showselected(new_selected)
  {
  console.log("selector: ", new_selected );
  this.hour=new_selected;
  console.log(this.hour);
  }
  serviceselected(service_selected)
  {
  console.log("selector: ", service_selected );
  this.service=service_selected;
  console.log(this.service);
  }
}
