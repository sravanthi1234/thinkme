import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NursePage } from './nurse';

@NgModule({
  declarations: [
    NursePage,
  ],
  imports: [
    IonicPageModule.forChild(NursePage),
  ],
})
export class NursePageModule {}
