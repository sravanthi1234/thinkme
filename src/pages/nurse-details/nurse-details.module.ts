import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NurseDetailsPage } from './nurse-details';

@NgModule({
  declarations: [
    NurseDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(NurseDetailsPage),
  ],
})
export class NurseDetailsPageModule {}
