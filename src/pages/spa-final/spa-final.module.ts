import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpaFinalPage } from './spa-final';

@NgModule({
  declarations: [
    SpaFinalPage,
  ],
  imports: [
    IonicPageModule.forChild(SpaFinalPage),
  ],
})
export class SpaFinalPageModule {}
