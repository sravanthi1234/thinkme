import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElderCarePage } from './elder-care';

@NgModule({
  declarations: [
    ElderCarePage,
  ],
  imports: [
    IonicPageModule.forChild(ElderCarePage),
  ],
})
export class ElderCarePageModule {}
