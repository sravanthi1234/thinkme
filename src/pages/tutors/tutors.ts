import { EducationPage } from './../education/education';
import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SportsPage } from '../sports/sports';

/**
 * Generated class for the TutorsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tutors',
  templateUrl: 'tutors.html',
})
export class TutorsPage {
  tab1Root = EducationPage;
  tab2Root = SportsPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad TutorsPage');
  }
  homeNew(){
    console.log("hdh")
    this.navCtrl.setRoot(HomePage);
    //this.navCtrl.popToRoot();
    
  }
}
