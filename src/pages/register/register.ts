import { LoginPage } from './../login/login';
import { ThinkmeProvider } from './../../providers/thinkme/thinkme';

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl,FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { ToastController } from 'ionic-angular';
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  main:any;
  name:any;
  email:any;
 
  mobile:any;
  password:any;
  confirmPassword:any;
  contactNumber:any;
  qualification:any;
  age:any;
  gender:any;
  categories= [
    {label:'Courier'},
    {label:'Events'},
    {label:'Elder Care'},
    {label:'Catering'}, 
    {label:'Tutor & Training'}, 
    {label:'Home Appliances'},  
    {label:'Maid Services'}, 
    {label:'Health & Fitness'}, 
  
   
   
  ];
  subCategories= [
    {label:'Ac Repair'},
    {label:'Gas Stove'},
    {label:'Geyser'},
  ];
  activities= [
    {label:'Repair and installation'},
    {label:'General Service'},
  ];
  loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  validation_messages = {
    'email': [
      { type: 'required', message: 'email is required.' },
      { type: 'pattern', message: 'Please include @ in email like example@gmail.com' },
  ],
  'name': [
    { type: 'required', message: 'Username is required.' },
  
],
'mobileNumber': [
  { type: 'required', message: 'mobile number is required.' },
  { type: 'maxlength', message: 'please enter 10 digits only.' },
  { type: 'pattern', message: 'please enter numbers only' }

  
],
'age': [
  { type: 'required', message: 'Age is is required.' }
 

],



}
public todo : FormGroup;
  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController,public usersService: ThinkmeProvider, public navParams: NavParams,private formBuilder: FormBuilder,public http: HttpClient,public toastCtrl: ToastController) {
    this.todo = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      name: new FormControl('', Validators.compose([
        Validators.required
       
      ])),
      mobileNumber: new FormControl('', Validators.compose([
        Validators.required,
        Validators.maxLength(10),
        Validators.pattern("^[1-9][0-9]*")
       
       
      ])),
     
      
      password: new FormControl('', [Validators.required]),
      re_password: new FormControl('', [Validators.required,this.equalto('password')]),
    },
   
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  equalto(field_name): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
    
    let input = control.value;
    
    let isValid=control.root.value[field_name]==input
    if(!isValid) 
    return { 'equalTo': {isValid} }
    else 
    return null;
    };
    }
    logForm() {
      
      //console.log(this.todo.value)
      let body = {
     "userName": this.todo.value.name,
       "password":this.todo.value.re_password,
      "role":"user",
      "emailId": this.todo.value.email,
    "phone":parseInt(this.todo.value.mobileNumber),
 
    }
    console.log(body);


this.usersService.addUser('http://ec2-54-84-142-57.compute-1.amazonaws.com:6060/user/registeruser',body).subscribe(res => {
  console.log(JSON.stringify(res["message"]));
  let responseMessage = res["message"];
  if(responseMessage == "user resgistered succesfully"){
    let toast = this.toastCtrl.create({
      message: responseMessage,
      duration: 3000,
      cssClass: "yourCssClassName",
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      this.loading.dismiss();
      this.navCtrl.setRoot(LoginPage);
    });
this.loading.present();
    toast.present();
  }else{

  }
})



    
    }
  showselected(new_selected)
  {
  console.log("selector: ", new_selected );
  this.main=new_selected;
  console.log(this.main);
 
  }
  Register(){
    
   
  }
}
