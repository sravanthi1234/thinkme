import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CookingDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cooking-details',
  templateUrl: 'cooking-details.html',
})
export class CookingDetailsPage {
  hour:any;
  service:any;
  age:any;
  area:any;
  gender:any;
  location:any;
  date:any;
  variety:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.variety=this.navParams.get("category");
    this.service=this.navParams.get("category1");
    this.date=this.navParams.get("category2");
  
    this.gender=this.navParams.get("category3");
    this.location=this.navParams.get("category4");
    console.log(this.location);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CookingDetailsPage');
  }

}
