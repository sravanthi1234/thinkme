import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CookingDetailsPage } from './cooking-details';

@NgModule({
  declarations: [
    CookingDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(CookingDetailsPage),
  ],
})
export class CookingDetailsPageModule {}
