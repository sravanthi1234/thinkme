import { BabyCareDetailsPage } from './../baby-care-details/baby-care-details';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { ElementRef, NgZone,ViewChild } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { AlertController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular'

import { CourierdetailsPage } from '../../pages/courierdetails/courierdetails';
import {CourierDetailsInfo} from '../../Model/CourierDetailsInfo';
import { MaidDetailsPage } from '../maid-details/maid-details';


@IonicPage()
@Component({
  selector: 'page-baby-care',
  templateUrl: 'baby-care.html',
})
export class BabyCarePage {

  hour:any;
  Religion:any;
  area:any;
  service:any;
  Gender:any;
  dateFinal:any;
  age:any;
  hours= [
    {label:'4 Hours'},
    {label:'8 Hours'},
    {label:'12 Hours'},
    {label:'24 Hours'},
  ];
  services= [
    {label:'Baby Bathing'},
    {label:'Baby Feeding'},
   
  
  ];
  form = [
    { val: 'Home Cleaning', isChecked: true },
    { val: 'Laundry', isChecked: false },
    { val: 'Dish Cleaning', isChecked: false }
  ];
  @ViewChild("dropAddress")
  public dropAddressElementRef: ElementRef;
  @ViewChild("pickUpAddress")
  public pickUpAddressElementRef: ElementRef;
  
  public zoom: number;
  distance: number = 0;
  fare: number = 0;
  model = new CourierDetailsInfo();

  constructor(private navCtrl:NavController,private geolocation: Geolocation, public mapsAPILoader:MapsAPILoader,private ngZone: NgZone, public alertCtrl: AlertController,public actionSheetCtrl: ActionSheetController) {
    
  }

  ngOnInit(){
    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
    this.model.fromLat=this.model.fromLat=data.coords.latitude;
    this.model.fromLng=this.model.fromLng=data.coords.longitude;
    var geocoder = new google.maps.Geocoder;
    // this.GetCurrentAddressText(geocoder,this.model.fromLat,this.model.fromLng);
    this.setCurrentPosition();

  });
      this.AutoCompleteMapTextbox();
  }



private setCurrentPosition() {
  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.model.fromLat = position.coords.latitude;
      this.model.fromLng = position.coords.longitude;
      this.zoom = 18;
    });
  }
}

async NormalCourier(){
    
    this.presentActionSheet();
}

showAlert() {
  const alert = this.alertCtrl.create({
    subTitle: 'Please Enter Valid Address!',
    buttons: ['OK']
  });
  alert.present();
}


presentActionSheet() {
  let actionSheet = this.actionSheetCtrl.create({
    title: 'Conform Your Address',
    buttons: [
      
      {
        text: 'Drop Address :-' + this.model.toAddress
      },
     
     
      {
        text: 'Conform',
        role: 'conform',cssClass:'btnConfirmClass',
        handler: () => {
          this.gotoCaender();
        }
      }
    ]
  });
  actionSheet.present();
}

gotoCaender(){
  this.navCtrl.push(BabyCareDetailsPage,{category:this.hour,category1:this.service,category2:this.age,category3:this.dateFinal,category4:this.Gender,category5:this.model.toAddress});
console.log(this.model.toAddress);
}

getDistance():Promise<number>{
  return new Promise((resolve,reject)=>{
  var service = new google.maps.DistanceMatrixService();
  service.getDistanceMatrix({
     // origins: [this.model.fromAddress],
      destinations: [this.model.toAddress],
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.METRIC,
      avoidHighways: false,
      avoidTolls: false
  }, function (response, status) {
      if (status == google.maps.DistanceMatrixStatus.OK) {
          var distance = (parseInt(response.rows[0].elements[0].distance.text));
          resolve(distance);
         // var duration = response.rows[0].elements[0].duration.text;

      } else {
          alert("Unable to find the distance via road.");
      }
  });
  
});
}


AutoCompleteMapTextbox(){
  this.mapsAPILoader.load().then(() => {
 
    let autocomplete = new google.maps.places.Autocomplete(this.dropAddressElementRef.nativeElement, {
      types: ["address"]
    });
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();
        this.model.toAddress=place.formatted_address;

        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        this.model.toLat = place.geometry.location.lat();
        this.model.toLag = place.geometry.location.lng();
        this.zoom = 12;
      });
    });
  });


  
  }

  logChange(event){
    console.log(event);
  }
  onChange(event) {
    
    console.log(event.format('DD-MM-YYYY')); // For actual usage.
   this.dateFinal=event.format('DD-MM-YYYY');
   console.log(this.dateFinal)
   // console.log(moment(event).format('DD-MM-YYYY')); // the statement you might think about
  }
  showselected(new_selected)
  {
  console.log("selector: ", new_selected );
  this.hour=new_selected;
  console.log(this.hour);
  }
  serviceselected(service_selected)
  {
  console.log("selector: ", service_selected );
  this.service=service_selected;
  console.log(this.service);
  }
}
