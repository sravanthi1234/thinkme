import { MaidDetailsPage } from './../maid-details/maid-details';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MaidPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-maid',
  templateUrl: 'maid.html',
})
export class MaidPage {
  hour:any;
  Religion:any;
  area:any;
  service:any;
  Gender:any;
  hours= [
    {label:'4 Hours'},
    {label:'8 Hours'},
    {label:'12 Hours'},
    {label:'24 Hours'},
  ];
  services= [
    {label:'Home Cleaning'},
    {label:'Laundry Services'},
    {label:'Dish wash'},
    
  ];
  form = [
    { val: 'Home Cleaning', isChecked: true },
    { val: 'Laundry', isChecked: false },
    { val: 'Dish Cleaning', isChecked: false }
  ];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
  }
  showselected(new_selected)
  {
  console.log("selector: ", new_selected );
  this.hour=new_selected;
  console.log(this.hour);
  }
  serviceselected(service_selected)
  {
  console.log("selector: ", service_selected );
  this.service=service_selected;
  console.log(this.service);
  }
  maidServices(){
   // this.navCtrl.push(MaidDetailsPage,{category:this.hour,category1:this.service,category2:this.Religion,category3:this.area,category4:this.Gender});
  
    console.log(this.hour);
    console.log(this.service);
    console.log(this.Religion);
    console.log(this.area);
console.log(this.Gender);

}
  ionViewDidLoad() {
    console.log('ionViewDidLoad MaidPage');
  }

}
