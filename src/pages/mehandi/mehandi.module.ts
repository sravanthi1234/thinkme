import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MehandiPage } from './mehandi';
import { CalendarModule } from "ion2-calendar";

@NgModule({
  declarations: [
    MehandiPage,
  ],
  imports: [
    IonicPageModule.forChild(MehandiPage),CalendarModule,
  ],
})
export class MehandiPageModule {}
