import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BabyCareDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-baby-care-details',
  templateUrl: 'baby-care-details.html',
})
export class BabyCareDetailsPage {

  hour:any;
  service:any;
  age:any;
  area:any;
  gender:any;
  location:any;
  date:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.hour=this.navParams.get("category");
    this.service=this.navParams.get("category1");
    this.age=this.navParams.get("category2");
   this.date=this.navParams.get("category3");
    this.gender=this.navParams.get("category4");
    this.location=this.navParams.get("category5");
    console.log(this.location);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BabyCareDetailsPage');
  }

}
