import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BabyCareDetailsPage } from './baby-care-details';

@NgModule({
  declarations: [
    BabyCareDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(BabyCareDetailsPage),
  ],
})
export class BabyCareDetailsPageModule {}
