import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-booking',
  templateUrl: 'booking.html',
})
export class BookingPage {
  today = Date.now();
  tomorrow: Date = new Date();
  other: Date = new Date();
  after: Date = new Date();
  buttonColor: string = '#fff'; //Default Color
  buttonColor1: string = '#fff'; //Default Color
  buttonColor2: string = '#fff'; //Default Color
  buttonColor3: string = '#fff'; //Default Color
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tomorrow.setDate(this.tomorrow.getDate() + 1);
    this.other.setDate(this.other.getDate() + 2);
    this.after.setDate(this.after.getDate() + 3);
  }
  addEvent(){
    this.buttonColor = '#345465'; //desired Color
    this.buttonColor1 = '#fff'; //desired Color
    this.buttonColor2 = '#fff'; //desired Color
    this.buttonColor3 = '#fff'; //desired Color
    /*
    YOUR FUNCTION CODE
    */
    
    }
    addColor(){
      this.buttonColor1 = '#345465'; //desired Color
      this.buttonColor = '#fff'; //desired Color
      this.buttonColor2 = '#fff'; //desired Color
      this.buttonColor3 = '#fff'; //desired Color
      /*
      YOUR FUNCTION CODE
      */
      
      }
      thirdDay(){
        this.buttonColor1 = '#fff'; //desired Color
        this.buttonColor = '#fff'; //desired Color
        this.buttonColor2 = '#345465'; //desired Color
        this.buttonColor3 = '#fff'; //desired Color
        /*
        YOUR FUNCTION CODE
        */
        
        }
        fourthDay(){
          this.buttonColor1 = '#fff'; //desired Color
          this.buttonColor = '#fff'; //desired Color
           this.buttonColor2 = '#fff'; //desired Color
          this.buttonColor3 = '#345465'; //desired Color
          /*
          YOUR FUNCTION CODE
          */
          
          }
  ionViewDidLoad() {
    console.log('ionViewDidLoad BookingPage');
  }

}
