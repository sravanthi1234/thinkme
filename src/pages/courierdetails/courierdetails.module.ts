import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CourierdetailsPage } from './courierdetails';

@NgModule({
  declarations: [
    CourierdetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(CourierdetailsPage),
  ],
})
export class CourierdetailsPageModule {}
