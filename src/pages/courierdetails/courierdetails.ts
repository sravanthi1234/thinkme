import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';

import {CourierDetailsInfo} from '../../Model/CourierDetailsInfo';
import { CourierDataProvider } from '../../providers/courier-data/courier-data';

@IonicPage()
@Component({
  selector: 'page-courierdetails',
  templateUrl: 'courierdetails.html',
})
export class CourierdetailsPage {

  model = new CourierDetailsInfo();
  today:string = new Date().toJSON().split('T')[0];

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController, public _courierDataProvider:CourierDataProvider) {
    this.model = navParams.get('courierInfo');
  }


  saveCourierData(){
    this._courierDataProvider.saveCourierData(this.model).subscribe(item=>{
      console.log('Added Successfully');
    });
    this.showAlert();
 }

  showAlert() {
    console.log(this.model);

    const alert = this.alertCtrl.create({
      subTitle: 'Under Development!',
      buttons: ['OK']
    });
    alert.present();
  }

}
