import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhotographyPage } from './photography';
import { CalendarModule } from "ion2-calendar";

@NgModule({
  declarations: [
    PhotographyPage,
  ],
  imports: [
    IonicPageModule.forChild(PhotographyPage),CalendarModule,
  ],
})
export class PhotographyPageModule {}
