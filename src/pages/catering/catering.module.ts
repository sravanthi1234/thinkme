import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CateringPage } from './catering';
import { CalendarModule } from "ion2-calendar";


@NgModule({
  declarations: [
    CateringPage,
  ],
  imports: [
    IonicPageModule.forChild(CateringPage),CalendarModule,
  ],
})
export class CateringPageModule {}
