import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DjPage } from './dj';
import { CalendarModule } from "ion2-calendar";


@NgModule({
  declarations: [
    DjPage,
  ],
  imports: [
    IonicPageModule.forChild(DjPage),CalendarModule,
  ],
})
export class DjPageModule {}
