import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the OngoingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ongoing',
  templateUrl: 'ongoing.html',
})
export class OngoingPage {
  today = Date.now();
  tomorrow: Date = new Date();
  other: Date = new Date();
  after: Date = new Date();
  buttonColor: string = '#fff'; //Default Color
  buttonColor1: string = '#fff'; //Default Color
  buttonColor2: string = '#fff'; //Default Color
  buttonColor3: string = '#fff'; //Default Color
  buttonColor4: string = '#fff'; //Default Color
  buttonColor5: string = '#fff'; //Default Color
  fontColor:string='#0c4393';
  fontColor1:string='#0c4393';
  fontColor2:string='#0c4393';
  fontColor3:string='#0c4393';
  fontColor4:string='#0c4393';
  allservices;any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tomorrow.setDate(this.tomorrow.getDate() + 1);
  
    this.other.setDate(this.other.getDate() + 2);
    this.after.setDate(this.after.getDate() + 3);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OngoingPage');
  }
  addEvent(){
    this.buttonColor = '#0c4393'; //desired Color
    this.buttonColor1 = '#fff'; //desired Color
    this.buttonColor2 = '#fff'; //desired Color
    this.buttonColor3 = '#fff'; //desired Color
    this.fontColor1='#fff';
    this.fontColor2='#0c4393';
    this.fontColor3='#0c4393';
    this.fontColor4='#0c4393';
    /*
    YOUR FUNCTION CODE
    */
   this.allservices= [
    {label:'31/01/2018', time:'3:00',username:'Mr.Asif',location:'madhapur',type:'Ac Repair'},
    {label:'31/01/2018', time:'10:00',username:'Mr.Asif',location:'madhapur',type:'Ac Repair'},
    {label:'31/01/2018', time:'15:00',username:'Mr.Asif',location:'madhapur',type:'Ac Repair'},
   
  ];

  

 
 
 


    }
    addColor(){
     
      this.buttonColor1 = '#0c4393'; //desired Color
      this.buttonColor = '#fff'; //desired Color
      this.buttonColor2 = '#fff'; //desired Color
      this.buttonColor3 = '#fff'; //desired Color
      
      this.fontColor1='#0c4393';
      this.fontColor2='#fff';
      this.fontColor3='#0c4393';
      this.fontColor4='#0c4393';
      /*
      YOUR FUNCTION CODE

      */
     this.allservices= [
      {label:'31/01/2018', time:'3:00',username:'Mr.Asif',location:'madhapur',type:'Ac Repair'},
      {label:'31/01/2018', time:'10:00',username:'Mr.Asif',location:'madhapur',type:'Ac Repair'},
      {label:'31/01/2018', time:'15:00',username:'Mr.Asif',location:'madhapur',type:'Ac Repair'},
     
    ];
 
  }
      
      thirdDay(){
        this.buttonColor1 = '#fff'; //desired Color
        this.buttonColor = '#fff'; //desired Color
        this.buttonColor2 = '#0c4393'; //desired Color
        this.buttonColor3 = '#fff'; //desired Color
        this.fontColor1='#0c4393';
        this.fontColor2='#0c4393';
        this.fontColor3='#fff';
        this.fontColor4='#0c4393';
        /*
        YOUR FUNCTION CODE
        */
      
       this.allservices= [
        {label:'31/01/2018', time:'3:00',username:'Mr.Asif',location:'madhapur',type:'Ac Repair'},
        {label:'31/01/2018', time:'10:00',username:'Mr.Asif',location:'madhapur',type:'Ac Repair'},
        {label:'31/01/2018', time:'15:00',username:'Mr.Asif',location:'madhapur',type:'Ac Repair'},
       
      ];
       
     
        }
        fourthDay(){
          this.buttonColor1 = '#fff'; //desired Color
          this.buttonColor = '#fff'; //desired Color
           this.buttonColor2 = '#fff'; //desired Color
          this.buttonColor3 = '#0c4393'; //desired Color
         
          this.fontColor1='#0c4393';
          this.fontColor2='#0c4393';
          this.fontColor3='#0c4393';
          this.fontColor4='#fff';
          /*
          YOUR FUNCTION CODE
          */
         this.allservices= [
          {label:'31/01/2018', time:'3:00',username:'Mr.Asif',location:'madhapur',type:'Ac Repair'},
          {label:'31/01/2018', time:'10:00',username:'Mr.Asif',location:'madhapur',type:'Ac Repair'},
          {label:'31/01/2018', time:'15:00',username:'Mr.Asif',location:'madhapur',type:'Ac Repair'},
         
        ];
        }
}
