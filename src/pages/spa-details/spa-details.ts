import { SpaPage } from './../spa/spa';
import { SpaFinalPage } from './../spa-final/spa-final';
import { BandDetailsPage } from './../band-details/band-details';
//import { BaratBandsPage } from './barat-bands';
import { WeddingCarDetailsPage } from './../wedding-car-details/wedding-car-details';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { ElementRef, NgZone,ViewChild } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { AlertController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular'

import { CourierdetailsPage } from '../../pages/courierdetails/courierdetails';
import {CourierDetailsInfo} from '../../Model/CourierDetailsInfo';
import { MaidDetailsPage } from '../maid-details/maid-details';

import { Storage } from '@ionic/storage';
@IonicPage()

@Component({
  selector: 'page-spa-details',
  templateUrl: 'spa-details.html',
})
export class SpaDetailsPage {

  hour:any;
  Religion:any;
  area:any;
  service:any;
  Gender:any;
  dateFinal:any;
  time:any;
  shoot:any;
 cost:any;
 label:any;
 package:any;
 wasteChecked: any = [];
 regularWax:any=[]
 allservices= [
  {label:'Half arms waxing Rs.230'},
  {label:'Half legs waxing  Rs.300'},
  {label:'Stomach waxing Rs. 350'},
  {label:'Back waxing Rs. 400'}  

 
];
regularWaxing= [
  {label:'Full arms (rica )+underarms (peal off)waxing  Rs.500'},
  {label:'Full legs (No B-Line and No Brazilian) Rs.500'},
  {label:'Full body wax (No B -wax) Rs.1100'},
  {label:'Underarms peal off wax Rs.150'},
  {label:'B-Line wax Rs.150'},
  {label:'B-wax Rs.699'}

 
];
facial= [
  {label:'Sara. Fruit clean up Rs 500'},
  {label:'Oil control treatment Rs.900'},
  {label:'Glovite facial Rs.950'},
  {label:'Tan clear  facial Rs.1200'},
  {label:'Feel youthful facial Rs.1200'},
  {label:'Shone &glow facial  Rs.1400'},
  {label:'Power brightening facial Rs.1500'}
];
pedicure= [
  {label:'Cut,file&polish -Feet. Rs.150'},
  {label:'Classic pedicure Rs.530'},
  {label:'Glovite facial Rs.950'},
  {label:'Spa pedicure Rs.699'},
  {label:'Detan pedicure Rs.750'},
];
maicure= [
  {label:'Cut,file&polish -Hand Rs150'},
  {label:'Classic manicure Rs.400'},
  {label:'Spa manicure Rs.500'},
  {label:'Spa pedicure Rs.699'},
  {label:'Detan manicure Rs.550'},
];
hair= [
  {label:'Henna application Rs.400'},
  {label:'Hair colour application Rs.500'},
  {label:'Root touch up-upto 2 inches-Rs.699'},
  {label:'Hair spa up to waist length Rs.900'},
  {label:'Head massage Rs.250'},
  {label:'Global hair colour upto waist length Rs.2699'},
  {label:'Global hair colour below waist length Rs.3299'},
];
threading= [
  {label:'Eyebrow Rs.25'},
  {label:'Upper lip Rs.15'},
  {label:'Chin rs.25'},
  {label:'Forehead Rs.35'},
  {label:'Side locks Rs.40'},
  {label:'Full face Rs.130'},
 
];
bleach= [
  {label:'Face &Neck bleach Rs.350'},
  {label:'Full arms bleach Rs.300'},
  {label:'Full legs bleach Ra.400'},
  {label:'Front bleach Rs.350'},
  {label:'Back bleach Rs.350'},
  {label:'Full body bleach Rs.1399'},
];
detan= [
  {label:'Face &Neck detan Rs.350'},
  {label:'Full arms detan Rs.300'},
  {label:'Full legs detan  Ra.400'},
  {label:'Front detan Rs.350'},
  {label:'Back detan Rs.350'},
  {label:'Full body detan  Rs.1399'},
];

  @ViewChild("dropAddress")
  public dropAddressElementRef: ElementRef;
  @ViewChild("pickUpAddress")
  public pickUpAddressElementRef: ElementRef;
  
  public zoom: number;
  distance: number = 0;
  fare: number = 0;
  model = new CourierDetailsInfo();

  constructor(private navCtrl:NavController,private storage: Storage,private geolocation: Geolocation, public mapsAPILoader:MapsAPILoader,private ngZone: NgZone, public alertCtrl: AlertController,public actionSheetCtrl: ActionSheetController, public navParams: NavParams) {
    
    this.label=this.navParams.get("category");
    
  }

  ngOnInit(){
    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
    this.model.fromLat=this.model.fromLat=data.coords.latitude;
    this.model.fromLng=this.model.fromLng=data.coords.longitude;
    var geocoder = new google.maps.Geocoder;
    // this.GetCurrentAddressText(geocoder,this.model.fromLat,this.model.fromLng);
    this.setCurrentPosition();

  });
      this.AutoCompleteMapTextbox();
      
  }



private setCurrentPosition() {
  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.model.fromLat = position.coords.latitude;
      this.model.fromLng = position.coords.longitude;
      this.zoom = 18;
    });
  }
}

async NormalCourier(){
    
    this.presentActionSheet();
}

showAlert() {
  const alert = this.alertCtrl.create({
    subTitle: 'Please Enter Valid Address!',
    buttons: ['OK']
  });
  alert.present();
}


presentActionSheet() {
  let actionSheet = this.actionSheetCtrl.create({
    title: 'Conform Your Address',
    buttons: [
      
      {
        text: 'Drop Address :-' + this.model.toAddress
      },
     
     
      {
        text: 'Conform',
        role: 'conform',cssClass:'btnConfirmClass',
        handler: () => {
          this.gotoCaender();
        }
      }
    ]
  });
  actionSheet.present();
}


gotoCaender(){
  this.navCtrl.push(SpaPage,{category1:this.time,category2:this.dateFinal,category3:this.model.toAddress,category4:this.label,category5:this.wasteChecked});

  console.log(this.model.toAddress);
console.log(this.time);
}

getDistance():Promise<number>{
  return new Promise((resolve,reject)=>{
  var service = new google.maps.DistanceMatrixService();
  service.getDistanceMatrix({
     // origins: [this.model.fromAddress],
      destinations: [this.model.toAddress],
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.METRIC,
      avoidHighways: false,
      avoidTolls: false
  }, function (response, status) {
      if (status == google.maps.DistanceMatrixStatus.OK) {
          var distance = (parseInt(response.rows[0].elements[0].distance.text));
          resolve(distance);
         // var duration = response.rows[0].elements[0].duration.text;

      } else {
          alert("Unable to find the distance via road.");
      }
  });
  
});
}


AutoCompleteMapTextbox(){
  this.mapsAPILoader.load().then(() => {
 
    let autocomplete = new google.maps.places.Autocomplete(this.dropAddressElementRef.nativeElement, {
      types: ["address"]
    });
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();
        this.model.toAddress=place.formatted_address;

        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        this.model.toLat = place.geometry.location.lat();
        this.model.toLag = place.geometry.location.lng();
        this.zoom = 12;
      });
    });
  });


  
  }

  logChange(event){
    console.log(event);
  }
  onChange(event) {
    
    console.log(event.format('DD-MM-YYYY')); // For actual usage.
   this.dateFinal=event.format('DD-MM-YYYY');
   console.log(this.dateFinal)
   // console.log(moment(event).format('DD-MM-YYYY')); // the statement you might think about
  }
  showselected(new_selected)
  {
  console.log("selector: ", new_selected );
  this.package=new_selected;
  console.log(this.package);
  }
  serviceselected(service_selected)
  {
  console.log("selector: ", service_selected );
  this.service=service_selected;
  console.log(this.service);
  }
  datachanged(e:any,data){
    //console.log(e);
    
    console.log(data);
    if(e.checked==true) {
      console.log("dd")
      this.wasteChecked.push(data);
      console.log(this.wasteChecked);
  }else if(e.checked==false){
    let index = this.wasteChecked.indexOf(data);
      console.log(index + "this is index"+data);
      // if (index == -1) {
        this.wasteChecked.splice(index, 1);
        console.log(this.wasteChecked);
      // }

  }
 
  this.storage.set('regular', this.wasteChecked);
 
}
regular(e:any,data){
  //console.log(e);
 
  console.log(data);
  if(e.checked==true) {
    console.log("dd");
    this.regularWax.push(data);
    this.wasteChecked.push(data);
    console.log(this.regularWax);
    console.log(this.wasteChecked);
}else if(e.checked==false){
  let index = this.regularWax.indexOf(data);
    console.log(index + "this is index"+data);
    // if (index == -1) {
      this.regularWax.splice(index, 1);
      console.log(this.regularWax);
    // }

}
this.storage.set('rica', this.regularWax);
}

spaFinal(){
  this.navCtrl.push('SpaPage',{category4:this.label,category5:this.wasteChecked,category6:this.regularWax});

}
}
