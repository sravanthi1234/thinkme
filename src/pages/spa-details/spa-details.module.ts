import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpaDetailsPage } from './spa-details';
import { CalendarModule } from "ion2-calendar";


@NgModule({
  declarations: [
    SpaDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(SpaDetailsPage),CalendarModule,
  ],
})
export class SpaDetailsPageModule {}
