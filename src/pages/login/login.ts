import { ThinkmeProvider } from './../../providers/thinkme/thinkme';
import { SubHomePage } from './../sub-home/sub-home';
import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,AlertController, ModalController, } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  username: string = "";
  password: string = "";
  loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  logindata: any;
  constructor(public navCtrl: NavController,private storage: Storage, public navParams: NavParams, public alertCtrl: AlertController, public usersService: ThinkmeProvider, public http: HttpClient,public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  login(){

    if (this.username == "" || this.password == "") {
      let userAlert = this.alertCtrl.create({

        subTitle: 'Please enter email ID/Username and Password',
        buttons: ['OK']
      });
      userAlert.present();
    }else {
      this.loading.present();
      let body = {
        "emailid": this.username,
        "password": this.password
      }

     console.log(body);

   this.usersService.addUser('http://ec2-54-84-142-57.compute-1.amazonaws.com:6060/user/loginuser',body).subscribe(res => {
      console.log(res);
      this.logindata = res;
      this.loading.dismiss();
    
      let userEmail = res["emailid"];
      let userId = res["userid"];
   //  
      var userid = localStorage.getItem("userId");
      // console.log(localStorage.getItem("userId"));
      // console.log(this.logindata.user_data.userid);
      if(res["success"] == true){
        
            this.navCtrl.setRoot(HomePage);
            this.storage.set('useremail',this.logindata.user_data.emailid);
      this.storage.set('username',this.logindata.user_data.username);
      localStorage.setItem('username',this.logindata.user_data.username);
     this.storage.set('phone',this.logindata.user_data.phone);
    console.log(this.logindata.user_data.username);
     console.log(this.logindata.user_data.emailid);
      localStorage.setItem("userId",this.logindata.user_data.userid);
         
      } else if (res["success"] == false) {
        console.log("puppyis here");
        this.loading.dismiss();
        console.log(res);
        let basicAlert = this.alertCtrl.create({

          subTitle: res["msg"],
          buttons: ['OK']
        });
        basicAlert.present();
      }
    })

  }

    //this.navCtrl.setRoot(HomePage);
  }
  signUp(){
    this.navCtrl.setRoot('RegisterPage');
  }
}
