import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DecorationDetailsPage } from './decoration-details';

@NgModule({
  declarations: [
    DecorationDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(DecorationDetailsPage),
  ],
})
export class DecorationDetailsPageModule {}
