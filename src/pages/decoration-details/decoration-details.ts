import { HomePage } from './../home/home';
import { ThinkmeProvider } from './../../providers/thinkme/thinkme';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { constructDependencies } from '@angular/core/src/di/reflective_provider';

import { ToastController } from 'ionic-angular';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the DecorationDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-decoration-details',
  templateUrl: 'decoration-details.html',
})
export class DecorationDetailsPage {
  serviceType:any;
  decorType:any;
  location:any;
  budget:any;
  username:any;
  phoneNumber:any;
  useremail:any;
  area:any;
  date:any;
  time:any;
  constructor(public navCtrl: NavController,private storage: Storage, private alertCtrl: AlertController,public loadingCtrl: LoadingController,public usersService: ThinkmeProvider, public navParams: NavParams,public http: HttpClient,public toastCtrl: ToastController) {
    this.serviceType=this.navParams.get("category");
    this.decorType=this.navParams.get("category1");
    this.location=this.navParams.get("category2");
   this.budget=this.navParams.get("category3");
   this.area=this.navParams.get("area");
   this.date=this.navParams.get("date");
   this.time=this.navParams.get("time");

   
   console.log(this.area);
   this.storage.get('username').then((val) => {
    console.log('Your name is', val);
    this.username=val;
  });
  this.storage.get('useremail').then((val) => {
    console.log('Your email is', val);
    this.useremail=val;
  });
  this.storage.get('phone').then((val) => {
    console.log('Your phone Number is', val);
    this.phoneNumber=val;
  });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DecorationDetailsPage');
  }
  logForm() {
      
    //console.log(this.todo.value)
    let body ={
      "userid":this.username,
      "eventCategory": "Decorations",
     "mainCategory": "Events",
      "subCategory":"Decorations", 
      "Date":this.date,
     "Time":this.time,   
 "bookingData":
      {
"mainCategory": "Events",
"subCategory":"Decorations",
"userEmail": this.useremail,
"phoneNumber":  this.phoneNumber,
"chooseEvent":this.serviceType,
"location":this.location,
"area":this.area,
"Date":this.date,
"Time":this.time,
"decorationType":this.decorType

        
      }
      }
      
  console.log(body);


this.usersService.bookingUser(body).subscribe(res => {
console.log(JSON.stringify(res["Message"]));
console.log("demo");
console.log(res);
let responseMessage = res["Message"];
if(responseMessage =="We accept your request, After vendor confirmation we will update you"){
  // let toast = this.toastCtrl.create({
  //   message: responseMessage,
  //   duration: 3000,
  //   cssClass: "yourCssClassName",
  //   position: 'bottom'
  // });
  let alert = this.alertCtrl.create({
    title: 'Thank you.!!',
    subTitle: responseMessage,
    buttons: ['ok']
  });
  alert.onDidDismiss(() => {
    // this.loading.dismiss();
    this.navCtrl.setRoot(HomePage);
   });
//this.loading.present();
alert.present();
}else{

}
})



  
  }
}
