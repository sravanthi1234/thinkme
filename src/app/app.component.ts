import { MaidNewPage } from './../pages/maid-new/maid-new';
import { MaidPage } from './../pages/maid/maid';
import { LoginPage } from './../pages/login/login';
import { RegisterPage } from './../pages/register/register';
import { HomePage } from './../pages/home/home';
import { CateringPage } from './../pages/catering/catering';
import { TutorsPage } from './../pages/tutors/tutors';
import { ServiceInfoPage } from './../pages/service-info/service-info';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform,Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AboutPage } from '../pages/about/about';
import { MybookingsPage } from '../pages/mybookings/mybookings';
import { SupportPage } from '../pages/support/support';
import { BookingPage } from '../pages/booking/booking';
import { SubCategoryPage } from '../pages/sub-category/sub-category';
import { LocationComponent } from '../components/location/location';
import { DetailsPage } from '../pages/details/details';
import { Storage } from '@ionic/storage';
import { SplashPage } from '../pages/splash/splash';
//import { LocationPage } from '../pages/location/location';
import {  ModalController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'LoginPage';
username:any = localStorage.getItem("username");
useremail:any;
phoneNumber:any;
  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform,modalCtrl: ModalController,public menuCtrl: MenuController, public statusBar: StatusBar ,private storage: Storage,public splashScreen: SplashScreen, public events:Events) {
    
     this.initializeApp();

    this.username = localStorage.getItem("username");
    console.log(this.username);
    // this.storage.get('useremail').then((val) => {
    //   console.log('Your email is', val);
    //   this.useremail=val;
    // });
    this.storage.get('phone').then((val) => {
      console.log('Your phone Number is', val);
      this.phoneNumber=val;
    });
    // this.storage.get('username').then((val) => {
    //   console.log('Your phone Number is', val);
    //   this.username=val;
    // });
   
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'My Bookings', component: MybookingsPage },
      { title: 'About', component: AboutPage },
      { title: 'Support', component: SupportPage },
      { title: 'Logout', component: SupportPage },
      
    ];
   
  }

  ngOnInit() {
    // this.storage.get('username').then((val) => {
    //   console.log('Your name is', val);
    //   this.username=val;
    // });
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

 home(){
  this.nav.setRoot(HomePage);
  this.menuCtrl.close();
 }
  openPage(page) {
    this.nav.setRoot(page.component);
  }
  logout(){
    this.nav.setRoot(LoginPage);
    this.menuCtrl.close();
  
   // this.storage.clear();
    this.storage.remove('emailid');
    this.storage.remove('username');
    localStorage.removeItem('username');
  }
  menuClosed() {
    this.events.publish('menu:closed', '');
  }
  menuOpened() {
    this.events.publish('menu:opened', '');
  }
}
