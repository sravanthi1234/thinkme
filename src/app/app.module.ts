import { HomePage } from './../pages/home/home';





import { LocationComponent } from './../components/location/location';
//import { LocationPage } from './../pages/location/location';

import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpClientModule } from '@angular/common/http'; 
import {HttpModule } from '@angular/http'

//imports
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AgmCoreModule } from '@agm/core';
import { Geolocation } from '@ionic-native/geolocation';
//component
import {ServicesComponent} from '../components/services/services'
import {CourierComponent} from '../components/courier/courier';
import { CalendarModule } from "ion2-calendar";

import * as moment from 'moment';
//pages



import { CourierDataProvider } from '../providers/courier-data/courier-data';
import { ServiceInfoPage } from '../pages/service-info/service-info';

import {  CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { IonicStorageModule } from '@ionic/storage';



import { ThinkmeProvider } from '../providers/thinkme/thinkme';
import { ThankuPage } from '../pages/thanku/thanku';




@NgModule({
  declarations: [
    MyApp,CourierComponent,ServicesComponent,
   
    ServiceInfoPage,LocationComponent,HomePage,
  ],
  imports: [
    BrowserModule,HttpClientModule,HttpModule,CalendarModule,
   
    IonicModule.forRoot(MyApp,{tabsPlacement: 'top',mode: 'md', tabsHideOnSubPages: true,}),
    IonicStorageModule.forRoot(),
   
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyArsQPH5d81K0Ekj-55t6ZbU866gUgeRgk',
      libraries: ['geometry', 'places']
    }),
   
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CourierComponent, ServiceInfoPage,HomePage,ServicesComponent
    
  ],
  providers: [
    StatusBar,
    SplashScreen,Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CourierDataProvider,
    
    ThinkmeProvider
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA ,NO_ERRORS_SCHEMA],
})
export class AppModule {}
