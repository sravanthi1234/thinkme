export class CourierDetailsInfo {
    public fromLat: number=0.0;
    public fromLng: number=0.0;
    public toLat: number=0.0;
    public toLag: number=0.0;
    public fromAddress: string='';
    public toAddress: string = '';
    public distance:number=0;
    public pickupDate:Date=new Date();
    public courierCharge:number=0;
    public itemWeight:number=0;
    public isItembreckable:boolean=false;
    public itemDescription:string='';
}