import { Component } from '@angular/core';
import { NavController,Events} from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { ElementRef, NgZone,ViewChild } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { AlertController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular'

import { CourierdetailsPage } from '../../pages/courierdetails/courierdetails';
import {CourierDetailsInfo} from '../../Model/CourierDetailsInfo';
import { Platform } from 'ionic-angular';


@Component({
  selector: 'courier',
  templateUrl: 'courier.html'
})

export class CourierComponent {

  @ViewChild("dropAddress")
  public dropAddressElementRef: ElementRef;
  @ViewChild("pickUpAddress")
  public pickUpAddressElementRef: ElementRef;
  
  public zoom: number;
  distance: number = 0;
  fare: number = 0;
  model = new CourierDetailsInfo();


  constructor(public plt: Platform,private navCtrl:NavController,private geolocation: Geolocation,public events:Events,
     public mapsAPILoader:MapsAPILoader,private ngZone: NgZone, public alertCtrl: AlertController,public actionSheetCtrl: ActionSheetController) {
    this.events.subscribe('menu:opened', () => {

    });
    
    this.events.subscribe('menu:closed', () => {
      debugger;
      let view = this.navCtrl.getActive();
     if(view.component.name=="HomePage" && this.model.fromLat>0){
      this.showmap(this.model.fromLat,this.model.fromLng);
       }
    });
  }

  ngOnInit(){
    // this.plt.ready().then((readySource) => {    
    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
    this.model.fromLat=data.coords.latitude;
    this.model.fromLng=data.coords.longitude;
    this.showmap(this.model.fromLat,this.model.fromLng);
    var geocoder = new google.maps.Geocoder;
     this.GetCurrentAddressText(geocoder,this.model.fromLat,this.model.fromLng);
    this.setCurrentPosition();
  });
  // var geocoder = new google.maps.Geocoder();

  //     // Try HTML5 geolocation.
  //     if (navigator.geolocation) {
  //       navigator.geolocation.getCurrentPosition(function(position) {
  //         debugger;
  //           this.model.fromLat= position.coords.latitude;
  //           this.model.fromLng= position.coords.longitude;
  //           this.showmap(this.model.fromLat,this.model.fromLng);
  //           var geocoder = new google.maps.Geocoder;
  //            this.GetCurrentAddressText(geocoder,this.model.fromLat,this.model.fromLng);
  //           this.setCurrentPosition();

  //         alert(position.coords.latitude);
  //       }, function() {
  //         this.handleLocationError(true, );
  //       });
  //     } else {
  //       // Browser doesn't support Geolocation
  //       this.handleLocationError(false);
  //     }

      this.AutoCompleteMapTextbox();
}

handleLocationError(browserHasGeolocation) {

alert(browserHasGeolocation);
}

  showmap(latitude,longitude) {
    var locationArray = new Array();
    const location = new google.maps.LatLng(latitude,longitude);
        const option = {
            center: location,
            zoom: 18,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          }
  
    const map = new google.maps.Map(document.getElementById('courierMap'), option);
  
    var marker = new google.maps.Marker({
        position: location,
        map: map,
        title: 'My Current Location!'
      });
  
      marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
    

      //------------ Mark Location-------------
      var locations = [
        ['Home', 12.8968469,77.6505845, 0],
        ['Baby Office', 12.8454445,77.6628762, 1],
        ['Office',12.9133802,77.6354227, 2],
      ];

      var infowindow = new google.maps.InfoWindow({});
  
      var i;
    
      for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(+locations[i][1], +locations[i][2]),
          map: map
        });
    
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
          return function () {
            infowindow.setContent(locations[i][0].toString());
            infowindow.open(map, marker);
          }
        })(marker, i));

        var myLatlng = new google.maps.LatLng(+locations[i][1], +locations[i][2]);
        locationArray.push(myLatlng);
      }
      //---------------Draw Route--------------------
      var directionsDisplay = new google.maps.DirectionsRenderer();
      var directionsService = new google.maps.DirectionsService();
      var start = new google.maps.LatLng(12.8454445,77.6628762);
      var end1 = new google.maps.LatLng(13.0974454,77.8315726);
      var end = new google.maps.LatLng(13.19864,77.7044041);
    
      var bounds = new google.maps.LatLngBounds();
      bounds.extend(start);
      bounds.extend(end1);
      bounds.extend(end);
      map.fitBounds(bounds);
      var request = {
          origin: start,
          destination: end,
          travelMode: google.maps.TravelMode.DRIVING
      };
      directionsService.route(request, function (response, status) {
          if (status == google.maps.DirectionsStatus.OK) {
              directionsDisplay.setDirections(response);
              directionsDisplay.setMap(map);
          } else {
              alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
          }
      });

  }


GetCurrentAddressText(geocoder, latitude,longitude) {
  geocoder.geocode({'location': {lat: latitude, lng: longitude }}, (results, status) => {
    if (status === 'OK') {
      if (results[0]) {
        this.model.fromAddress=results[0].formatted_address;

      } else {
        window.alert('No results found');
      }
    } else {
      window.alert('Geocoder failed due to: ' + status);
    }
  });
}


private setCurrentPosition() {
  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.model.fromLat = position.coords.latitude;
      this.model.fromLng = position.coords.longitude;
      this.zoom = 18;
    });
  }
}

async NormalCourier(){
    if(this.model.toLag <=0 || this.model.fromLng <=0){
      this.showAlert();
      return false;
    } 
    this.model.distance= await this.getDistance();
    this.model.courierCharge=this.model.distance*10;
    this.presentActionSheet();
}

showAlert() {
  const alert = this.alertCtrl.create({
    subTitle: 'Please Enter Valid Address!',
    buttons: ['OK']
  });
  alert.present();
}


presentActionSheet() {
  let actionSheet = this.actionSheetCtrl.create({
    title: 'Confirm Your Address',
    buttons: [
      {
        text: 'Pickup Address :-' + this.model.fromAddress
      },
      {
        text: 'Drop Address :-' + this.model.toAddress
      },
      {
        text:'Distance :-' + this.model.distance
      },
      {
        text:'Fare :-' + this.model.courierCharge
      },
      {
        text: 'Confirm',
        role: 'conform',cssClass:'btnConfirmClass',
        handler: () => {
          this.gotoCaender();
        }
      }
    ]
  });
  actionSheet.present();
}


gotoCaender(){
  this.navCtrl.push(CourierdetailsPage,{
    courierInfo: this.model
  });
}

getDistance():Promise<number>{
  return new Promise((resolve,reject)=>{
  var service = new google.maps.DistanceMatrixService();
  service.getDistanceMatrix({
      origins: [this.model.fromAddress],
      destinations: [this.model.toAddress],
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.METRIC,
      avoidHighways: false,
      avoidTolls: false
  }, function (response, status) {
      if (status == google.maps.DistanceMatrixStatus.OK) {
          var distance = (parseInt(response.rows[0].elements[0].distance.text));
          resolve(distance);
         // var duration = response.rows[0].elements[0].duration.text;

      } else {
          alert("Unable to find the distance via road.");
      }
  });
  
});
}


AutoCompleteMapTextbox(){
  this.mapsAPILoader.load().then(() => {
 
    let autocomplete = new google.maps.places.Autocomplete(this.dropAddressElementRef.nativeElement, {
      types: ["address"]
    });
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();
        this.model.toAddress=place.formatted_address;

        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        this.model.toLat = place.geometry.location.lat();
        this.model.toLag = place.geometry.location.lng();
        this.zoom = 12;
      });
    });
  });


  this.mapsAPILoader.load().then(() => {
 
    let autocomplete = new google.maps.places.Autocomplete(this.pickUpAddressElementRef.nativeElement, {
      types: ["address"]
    });
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();
        this.model.fromAddress=place.formatted_address;

        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        this.model.fromLat = place.geometry.location.lat();
        this.model.fromLng = place.geometry.location.lng();
        this.zoom = 12;
      });
    });
  });
  }

  logChange(event){
    console.log(event);
  }

}
