import { SpaPage } from './../../pages/spa/spa';
import { ElderCarePage } from './../../pages/elder-care/elder-care';
import { CateringPage } from './../../pages/catering/catering';
import { SubCategoryPage } from './../../pages/sub-category/sub-category';
import { Component } from '@angular/core';
import { Events, NavController } from 'ionic-angular';
import{CourierComponent} from '../../components/courier/courier';
import { ServiceInfoPage } from '../../pages/service-info/service-info';
import { AddressPage } from '../../pages/address/address';
import { LocationComponent } from '../location/location';
import { TutorsPage } from '../../pages/tutors/tutors';


@Component({
  selector: 'services',
  templateUrl: 'services.html'
})
export class ServicesComponent {
  
  
  
  constructor(public navCtrl: NavController,public  events:Events) {
  

  }

  nextpage(){
    //this.events.publish('menu:closed', '');
    this.navCtrl.push(CourierComponent);
  }
info(name){
  this.navCtrl.push('SubCategoryPage',{category:name});
  
}
tutors(){
  this.navCtrl.push('TutorsPage');
}
Catering(){
  this.navCtrl.push('CateringPage');
}
elder(){
  this.navCtrl.setRoot('ElderCarePage');
}
subCategory(){
  
}
spanew(){
  this.navCtrl.setRoot('SpaPage')
}
}
