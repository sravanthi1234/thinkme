import { NgModule } from '@angular/core';
import { CourierComponent } from './courier/courier';
import { ServicesComponent } from './services/services';
import { LocationComponent } from './location/location';
import { IonicApp, IonicModule } from 'ionic-angular';
import {  CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
@NgModule({
	declarations: [CourierComponent,
    ServicesComponent,
    LocationComponent],
	imports: [ IonicModule ],
	exports: [CourierComponent,
    ServicesComponent,
    LocationComponent],
    bootstrap: [ IonicApp ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule {}
