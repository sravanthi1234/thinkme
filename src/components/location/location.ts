import { AddressPage } from './../../pages/address/address';
import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { NavController,Events} from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { ElementRef, NgZone } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { AlertController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular'

//import { CourierdetailsPage } from '../../pages/courierdetails/courierdetails';
import {CourierDetailsInfo} from '../../Model/CourierDetailsInfo';
import { Platform } from 'ionic-angular';
import { ServiceInfoPage } from '../../pages/service-info/service-info';
import { DetailsPage } from '../../pages/details/details';
import { IonicPage, NavParams } from 'ionic-angular';

@Component({
  selector: 'location',
  templateUrl: 'location.html'
})
export class LocationComponent {
  dateone:any;
  datetwo:any;
  name:any;
  dateFinal:any;
  subheading=[];
  currentDate:any;
  message:string;
  today = Date.now();
  tomorrow: Date = new Date();
  other: Date = new Date();
  after: Date = new Date();
  selectedItem: string;
whatISelected: string;
buttonColornew: string;
  buttonColor: string = '#fff'; //Default Color
  buttonColor1: string = '#fff'; //Default Color
  buttonColor2: string = '#fff'; //Default Color
  buttonColor3: string = '#fff'; //Default Color
  buttonColor4: string = '#fff'; //Default Color
  buttonColor5: string = '#fff'; //Default Color
  fontColor:string='#0c4393';
  fontColor1:string='#0c4393';
  fontColor2:string='#0c4393';
  fontColor3:string='#0c4393';
  fontColor4:string='#0c4393';
  selectedId:number;
  @ViewChild("dropAddress")
  public dropAddressElementRef: ElementRef;
  @ViewChild("pickUpAddress")
  public pickUpAddressElementRef: ElementRef;
  
  
  public zoom: number;
  distance: number = 0;
  fare: number = 0;
  model = new CourierDetailsInfo();
  tomorrowDate: any;
  getDateTime: number;
  time: any;
  item: string;
  timeVal: string;
  newCategory: any;
  subCat: any;

  
  constructor(public plt: Platform,private navCtrl:NavController,private geolocation: Geolocation,public events:Events,public navParams: NavParams,
     public mapsAPILoader:MapsAPILoader,private ngZone: NgZone, public alertCtrl: AlertController,public actionSheetCtrl: ActionSheetController) {
    this.events.subscribe('menu:opened', () => {

    });
    
    this.events.subscribe('menu:closed', () => {
      debugger;
      let view = this.navCtrl.getActive();
     if(view.component.name=="HomePage" && this.model.fromLat>0){
      this.showmap(this.model.fromLat,this.model.fromLng);
       }
    });
   this.tomorrow.setDate(this.tomorrow.getDate() + 1);
  
    this.other.setDate(this.other.getDate() + 2);
    this.after.setDate(this.after.getDate() + 3);
   
   this.subCat= this.navParams.get("category");
  
   console.log(this.subCat)

  }

  ngOnInit(){
    
    // this.plt.ready().then((readySource) => {    
    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
    this.model.fromLat=data.coords.latitude;
    this.model.fromLng=data.coords.longitude;
    this.showmap(this.model.fromLat,this.model.fromLng);
    var geocoder = new google.maps.Geocoder;
     this.GetCurrentAddressText(geocoder,this.model.fromLat,this.model.fromLng);
    this.setCurrentPosition();
  });
  // var geocoder = new google.maps.Geocoder();

  //     // Try HTML5 geolocation.
  //     if (navigator.geolocation) {
  //       navigator.geolocation.getCurrentPosition(function(position) {
  //         debugger;
  //           this.model.fromLat= position.coords.latitude;
  //           this.model.fromLng= position.coords.longitude;
  //           this.showmap(this.model.fromLat,this.model.fromLng);
  //           var geocoder = new google.maps.Geocoder;
  //            this.GetCurrentAddressText(geocoder,this.model.fromLat,this.model.fromLng);
  //           this.setCurrentPosition();

  //         alert(position.coords.latitude);
  //       }, function() {
  //         this.handleLocationError(true, );
  //       });
  //     } else {
  //       // Browser doesn't support Geolocation
  //       this.handleLocationError(false);
  //     }

      this.AutoCompleteMapTextbox();
}

handleLocationError(browserHasGeolocation) {

alert(browserHasGeolocation);
}

  showmap(latitude,longitude) {
    var locationArray = new Array();
    const location = new google.maps.LatLng(latitude,longitude);
        const option = {
            center: location,
            zoom: 18,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          }
  
    const map = new google.maps.Map(document.getElementById('courierMap'), option);
  
    var marker = new google.maps.Marker({
        position: location,
        map: map,
        title: 'My Current Location!'
      });
  
      marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
    

      //------------ Mark Location-------------
      var locations = [
        ['Home', 12.8968469,77.6505845, 0],
        ['Baby Office', 12.8454445,77.6628762, 1],
        ['Office',12.9133802,77.6354227, 2],
      ];

      var infowindow = new google.maps.InfoWindow({});
  
      var i;
    
      for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(+locations[i][1], +locations[i][2]),
          map: map
        });
    
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
          return function () {
            infowindow.setContent(locations[i][0].toString());
            infowindow.open(map, marker);
          }
        })(marker, i));

        var myLatlng = new google.maps.LatLng(+locations[i][1], +locations[i][2]);
        locationArray.push(myLatlng);
      }
      //---------------Draw Route--------------------
      var directionsDisplay = new google.maps.DirectionsRenderer();
      var directionsService = new google.maps.DirectionsService();
      var start = new google.maps.LatLng(12.8454445,77.6628762);
      var end1 = new google.maps.LatLng(13.0974454,77.8315726);
      var end = new google.maps.LatLng(13.19864,77.7044041);
    
      var bounds = new google.maps.LatLngBounds();
      bounds.extend(start);
      bounds.extend(end1);
      bounds.extend(end);
      map.fitBounds(bounds);
      var request = {
          origin: start,
          destination: end,
          travelMode: google.maps.TravelMode.DRIVING
      };
      directionsService.route(request, function (response, status) {
          if (status == google.maps.DirectionsStatus.OK) {
              directionsDisplay.setDirections(response);
              directionsDisplay.setMap(map);
          } else {
              alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
          }
      });

  }


GetCurrentAddressText(geocoder, latitude,longitude) {
  geocoder.geocode({'location': {lat: latitude, lng: longitude }}, (results, status) => {
    if (status === 'OK') {
      if (results[0]) {
        this.model.fromAddress=results[0].formatted_address;

      } else {
        window.alert('No results found');
      }
    } else {
      window.alert('Geocoder failed due to: ' + status);
    }
  });
}


private setCurrentPosition() {
  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.model.fromLat = position.coords.latitude;
      this.model.fromLng = position.coords.longitude;
      this.zoom = 18;
    });
  }
}


async NormalCourier(){
 //this.navCtrl.push(AddressPage)
  
    this.presentActionSheet();
}

showAlert() {
  const alert = this.alertCtrl.create({
    subTitle: 'Please Enter Valid Address!',
    buttons: ['OK']
  });
  alert.present();
}


presentActionSheet() {
  let actionSheet = this.actionSheetCtrl.create({
    title: 'Confirm Your Address',
    buttons: [
      {
        text: 'Your Address :-' + this.model.fromAddress
      },
      {
        text: 'Time :-' + this.timeVal
      },
      {
        text: 'Date :-' + this.dateFinal
      },
     
    
      
      {
        text: 'Confirm',
        role: 'conform',cssClass:'btnConfirmClass',
        handler: () => {
          this.gotoCaender();
        }
      }
    ]
  });
  actionSheet.present();
}


gotoCaender(){
  this.navCtrl.push(DetailsPage,{
    courierInfo: this.model.fromAddress,
    param1:this.dateFinal,param2:this.timeVal,param3:this.subCat
  });
 
}

getDistance():Promise<number>{
  return new Promise((resolve,reject)=>{
  var service = new google.maps.DistanceMatrixService();
  service.getDistanceMatrix({
      origins: [this.model.fromAddress],
      destinations: [this.model.toAddress],
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.METRIC,
      avoidHighways: false,
      avoidTolls: false
  }, function (response, status) {
      if (status == google.maps.DistanceMatrixStatus.OK) {
          var distance = (parseInt(response.rows[0].elements[0].distance.text));
          resolve(distance);
         // var duration = response.rows[0].elements[0].duration.text;

      } else {
          alert("Unable to find the distance via road.");
      }
  });
  
});
}


AutoCompleteMapTextbox(){
  

  this.mapsAPILoader.load().then(() => {
 
    let autocomplete = new google.maps.places.Autocomplete(this.pickUpAddressElementRef.nativeElement, {
      types: ["address"]
    });
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();
        this.model.fromAddress=place.formatted_address;

        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        this.model.fromLat = place.geometry.location.lat();
        this.model.fromLng = place.geometry.location.lng();
        this.zoom = 12;
      });
    });
  });
  }

  logChange(event){
    console.log(event);
  }
  addEvent(){
    this.buttonColor = '#0c4393'; //desired Color
    this.buttonColor1 = '#fff'; //desired Color
    this.buttonColor2 = '#fff'; //desired Color
    this.buttonColor3 = '#fff'; //desired Color
    this.fontColor1='#fff';
    this.fontColor2='#0c4393';
    this.fontColor3='#0c4393';
    this.fontColor4='#0c4393';
    /*
    YOUR FUNCTION CODE
    */
   

   this.getDateTime = new Date().getHours();
   
   var d = new Date(); 
   this.dateFinal=""+d.getFullYear()+"-"+d.getMonth()+1+"-"+(d.getDate());
 console.log(this.dateFinal);
    

  var num=22;
  this. subheading=[];
 
   while (num >this.getDateTime) {
    
    this.getDateTime = this.getDateTime +2;
    num--;
    console.log(this.getDateTime)
    let timeNew=this.getDateTime +".00"
    this.subheading.push({'time':timeNew});
 }
 
 
 


    }
    addColor(){
     
      this.buttonColor1 = '#0c4393'; //desired Color
      this.buttonColor = '#fff'; //desired Color
      this.buttonColor2 = '#fff'; //desired Color
      this.buttonColor3 = '#fff'; //desired Color
      
      this.fontColor1='#0c4393';
      this.fontColor2='#fff';
      this.fontColor3='#0c4393';
      this.fontColor4='#0c4393';
      /*
      YOUR FUNCTION CODE

      */
     this. subheading=[];
     let dateone=this.currentDate = new Date();
    let datetwo= this.tomorrow = new Date();
  this.tomorrow.setDate(this.tomorrow.getDate() + 1);
     console.log(dateone);
     console.log(datetwo)
     
     
     var d = new Date(); 
      this.dateFinal=""+d.getFullYear()+"-"+d.getMonth()+1+"-"+(d.getDate()+1);
    console.log(this.dateFinal);
       
    if (datetwo>dateone){
     
     
      this.subheading= [
        {time:'8.00'} ,
        {time:'10.00'} ,
        {time:'12.00'} ,
        {time:'14.00'} ,
        {time:'16.00'} ,
        {time:'18.00'} ,
        {time:'20.00'} ,
      
      ];
      
      return true;

    }
  }
      
      thirdDay(){
        this.buttonColor1 = '#fff'; //desired Color
        this.buttonColor = '#fff'; //desired Color
        this.buttonColor2 = '#0c4393'; //desired Color
        this.buttonColor3 = '#fff'; //desired Color
        this.fontColor1='#0c4393';
        this.fontColor2='#0c4393';
        this.fontColor3='#fff';
        this.fontColor4='#0c4393';
        /*
        YOUR FUNCTION CODE
        */
      
      
       this.subheading= [
        {time:'8.00'} ,
        {time:'10.00'} ,
        {time:'12.00'} ,
        {time:'14.00'} ,
        {time:'16.00'} ,
        {time:'18.00'} ,
        {time:'20.00'} ,
      
      ];
      
      var d = new Date(); 
      this.dateFinal=""+d.getFullYear()+"-"+d.getMonth()+1+"-"+(d.getDate()+2);
    console.log(this.dateFinal);
       
     
        }
        fourthDay(){
          this.buttonColor1 = '#fff'; //desired Color
          this.buttonColor = '#fff'; //desired Color
           this.buttonColor2 = '#fff'; //desired Color
          this.buttonColor3 = '#0c4393'; //desired Color
         
          this.fontColor1='#0c4393';
          this.fontColor2='#0c4393';
          this.fontColor3='#0c4393';
          this.fontColor4='#fff';
          /*
          YOUR FUNCTION CODE
          */
         this.subheading= [
          {time:'8.00'} ,
          {time:'10.00'} ,
          {time:'12.00'} ,
          {time:'14.00'} ,
          {time:'16.00'} ,
          {time:'18.00'} ,
          {time:'20.00'} ,
        
        ];
        var d = new Date(); 
         this.dateFinal=""+d.getFullYear()+"-"+d.getMonth()+1+"-"+(d.getDate()+3);
       console.log(this.dateFinal);
          }

          timeSelection(time,clicked,i,item){

           console.log(time);
           console.log(this.subheading.indexOf(clicked))
            
            if(this.subheading.indexOf(item)==i){
              clicked='true'
             console.log("dk")
              this.buttonColor4 = '#0c4393';

            }
            else {
            
            //  this.buttonColor4 = '#fff';
            }
            console.log(i);

            this.timeVal=time;
           
         
          // this.buttonColor4 = '#0c4393'; //desired Color
            this.fontColor='#fff'
          }
       
          btnActivate(id:number,time)
{
   this.selectedId= id;
   console.log(time);
   this.timeVal=time;
}
}
