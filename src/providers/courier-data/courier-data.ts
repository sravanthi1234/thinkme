import { HttpClient,HttpResponse, HttpHandler, HttpHeaders } from '@angular/common/http';
import { Injectable, DebugElement } from '@angular/core';
import {Observable} from 'rxjs/Rx';

import {Http,Response,Headers } from '@angular/http'


import {CourierDetailsInfo} from '../../Model/CourierDetailsInfo';

@Injectable()
export class CourierDataProvider {

  constructor(public http: HttpClient,public newHttp:Http) {

  }

saveCourierData(courierInfo:CourierDetailsInfo){
  debugger;
  let headers=new HttpHeaders();
  headers.append('content-Type','application/json');

  return this.http.post('http://localhost:3000/api/item',
  courierInfo,{headers:headers}).map((res: Response) => res);
}

getMyHistry(): Observable<CourierDetailsInfo[]>{
  return this.newHttp.get('http://localhost:3000/api/items')
  .map((res: Response) => <CourierDetailsInfo[]> res.json());

  // return this._http.get("http://localhost:51880/api/Employee")
  // .map((response: Response) => <EmployeeInfo[]>response.json())
  // .catch(this.handleError);

}
}
