import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../envinorments/envinorment';
/*
  Generated class for the ThinkmeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ThinkmeProvider {
public apiEndPoint = environment.apiEndPoint;
  constructor(public http: HttpClient) {
  
  }
  addUser(api,body){
    let httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    let options = {
      headers: httpHeaders
    };
    

    
    return this.http.post(api, JSON.stringify(body), options);

}
bookingUser(body){
  let httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
  });
  let options = {
    headers: httpHeaders
  };
  

  
  return this.http.post(this.apiEndPoint + "/user/booking", JSON.stringify(body), options);

}
}
